import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import '../dao/DiaryDao.dart';
import '../models/diary_entry.dart';
part 'diary_database.g.dart';


@Database(version: 1, entities: [DiaryEntry])
abstract class DiaryDatabase extends FloorDatabase {
  DiaryDao get diaryDao;
}
