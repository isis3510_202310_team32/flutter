// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'diary_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorDiaryDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$DiaryDatabaseBuilder databaseBuilder(String name) =>
      _$DiaryDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$DiaryDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$DiaryDatabaseBuilder(null);
}

class _$DiaryDatabaseBuilder {
  _$DiaryDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$DiaryDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$DiaryDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<DiaryDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$DiaryDatabase();
    //await sqflite.deleteDatabase(path); 

    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    
    return database;
  }
}

class _$DiaryDatabase extends DiaryDatabase {
  _$DiaryDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  DiaryDao? _diaryDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `diary` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT NOT NULL, `content` TEXT NOT NULL, `date` INTEGER NOT NULL, `restaurantId` TEXT NOT NULL, `restaurantName` TEXT NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  DiaryDao get diaryDao {
    return _diaryDaoInstance ??= _$DiaryDao(database, changeListener);
  }
}

class _$DiaryDao extends DiaryDao {
  _$DiaryDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _diaryEntryInsertionAdapter = InsertionAdapter(
            database,
            'diary',
            (DiaryEntry item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'content': item.content,
                  'date': item.timestamp,
                  'restaurantId': item.restaurantId,
                  'restaurantName': item.restaurantName
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<DiaryEntry> _diaryEntryInsertionAdapter;

  @override
  Future<List<DiaryEntry>> getAllEntries() async {
    return _queryAdapter.queryList('SELECT * FROM diary',
        mapper: (Map<String, Object?> row) => DiaryEntry(
            row['id'] as int?,
            row['title'] as String,
            row['content'] as String,
            row['date'] as int,
            row['restaurantId'] as String,
            row['restaurantName'] as String));
  }

  @override
  Future<void> insertEntry(DiaryEntry entry) async {
    await _diaryEntryInsertionAdapter.insert(entry, OnConflictStrategy.replace);
  }
}
