import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_oauth/firebase_auth_oauth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:senefood/models/restaurant.dart';
import 'package:senefood/models/user.dart';
import 'package:senefood/view_models/restaurant_service.dart';

class AuthUserService with ChangeNotifier {
  final CollectionReference _usersCollection =
      FirebaseFirestore.instance.collection("Users");
  List<AuthUser> allAppUsers = [];
  late AuthUser currentUser;
  bool userLogged = false;
  bool loadingFavoriteRestaurants = true;

  Future<void> setAUser(
      String? userId, String? newUserName, List<String>? prefs) async {
    final dataBase = FirebaseFirestore.instance;

    final user = <String, dynamic>{
      "preferences": prefs,
      "username": newUserName
    };

    await dataBase
        .collection("Users")
        .doc(userId)
        .set(user)
        .onError((e, _) => print("Error writing document: $e"));
  }

  Future<void> setUsersFavoriteRestaurants(
      String? userName, List<String>? favRestsIds) async {
    final dataBase = FirebaseFirestore.instance;

    final favRestaurants = <String, dynamic>{
      "restaurants": favRestsIds,
    };

    await dataBase
        .collection("FavouriteRestaurants")
        .doc(userName)
        .set(favRestaurants)
        .onError((e, _) => print("Error writing document: $e"));
  }

  Future<AuthUser> authenticate() async {
    try {
      if (!(await isUserLogged())) {
        User? user = await FirebaseAuthOAuth().openSignInFlow(
          "microsoft.com",
          [
            "https://graph.microsoft.com/User.Read",
            "https://graph.microsoft.com/User.ReadBasic.All"
          ],
        );
        if (user != null) {
          currentUser = AuthUser(
              id: user.uid,
              displayName: user.displayName,
              username: user.email,
              preferences: []);

          await setAUser(user.uid, user.email, []);
          await setUsersFavoriteRestaurants(user.email, []);
        }
      } else {
        currentUser = AuthUser(
            id: FirebaseAuth.instance.currentUser?.uid,
            displayName: FirebaseAuth.instance.currentUser?.displayName,
            username: FirebaseAuth.instance.currentUser?.email,
            preferences: []);
      }
      userLogged = true;
    } on PlatformException catch (error) {
      print(
          '================================= ERROR WHILE TRYING TO AUTHENTICATE =========================================');
      debugPrint("${error.code}: ${error.message}");
    }
    notifyListeners();
    return currentUser;
  }

  Future<void> getAllUsersProvider() async {
    QuerySnapshot querySnapshot = await _usersCollection.get();
    List<QueryDocumentSnapshot> snapshotList = querySnapshot.docs;
    List<AuthUser> users = [];
    for (QueryDocumentSnapshot snapshot in snapshotList) {
      dynamic dataUser = snapshot.data();
      AuthUser newUser = AuthUser(
          id: snapshot.id,
          preferences: dataUser['preferences'],
          username: dataUser['username']);
      users.add(newUser);
    }

    allAppUsers = users;
    notifyListeners();
  }

  Future<dynamic> getOneUser(String uid) async {
    DocumentReference document = _usersCollection.doc(uid);

    DocumentSnapshot snapshot = await document.get();

    return snapshot.data();
  }

  Future<void> signOut() async {
    userLogged = false;
    await FirebaseAuth.instance.signOut();
    notifyListeners();
  }

  Future<bool> isUserLogged() async {
    if (FirebaseAuth.instance.currentUser != null) {
      List<String> preferences = List<String>.from((await getOneUser(
              FirebaseAuth.instance.currentUser?.uid ?? ''))['preferences'] ??
          []);

      currentUser = AuthUser(
          id: FirebaseAuth.instance.currentUser?.uid,
          displayName: FirebaseAuth.instance.currentUser?.displayName,
          username: FirebaseAuth.instance.currentUser?.email,
          preferences: preferences);
      notifyListeners();
    }
    return FirebaseAuth.instance.currentUser != null;
  }

  AuthUser getUser(String? id) {
    return allAppUsers.firstWhere((AuthUser user) => user.id == id);
  }

  AuthUser getCurrentUser() {
    return currentUser;
  }

  Future<void> assignSelectedPreferences(List<String>? prefs) async {
    FirebaseFirestore db = FirebaseFirestore.instance;
    final userRef = db.collection("Users").doc(currentUser.getId);
    await userRef.update({"preferences": prefs}).then(
        (value) => print("DocumentSnapshot successfully updated!"),
        onError: (e) => print("Error updating document $e"));
    notifyListeners();
  }

  void changePreferences(List<String>? newPrefs) {
    currentUser.setPreferences(newPrefs);
  }

  void addFoodPreference(String preference) {
    currentUser.addPreferenceToList(preference);
  }

  
  Future<void> addNewFavoriteRestaurantToDB(String restId) async {
    FirebaseFirestore db = FirebaseFirestore.instance;
    final userFavRestsRef = db.collection("FavouriteRestaurants").doc(currentUser.getUserName);
    await userFavRestsRef.update({"restaurants" : FieldValue.arrayUnion([restId])});
    notifyListeners();
  }

  Future<void> removeFavoriteRestaurantFromDB(String restId) async {
    FirebaseFirestore db = FirebaseFirestore.instance;
    final userFavRestsRef = db.collection("FavouriteRestaurants").doc(currentUser.getUserName);
    await userFavRestsRef.update({"restaurants" : FieldValue.arrayRemove([restId])});
    notifyListeners();
  }

  void addFavoriteRestaurant(String id){
    currentUser.addRestaurantToFavorites(id);
  }

  bool isFavoriteRestaurant(String id){
    return currentUser.getFavoriteRestaurants!.contains(id);
  }

  void removeFavoriteRestaurant(String id){
    currentUser.removeRestaurantFromFavorites(id);
  }

  Future<List<String>> getFavoriteRestaurantsIdsFromDB() async {
    final CollectionReference favoriteRestaurantsCollection = 
      FirebaseFirestore.instance.collection("FavouriteRestaurants");
    QuerySnapshot querySnapshot = await favoriteRestaurantsCollection.get();
    RestaurantService restaurantService =RestaurantService();
      final source =
          (querySnapshot.metadata.isFromCache) ? "local cache" : "server";
      print("Data fetched from $source}");
      List<QueryDocumentSnapshot> snapshotList = querySnapshot.docs;
      List<Restaurant> res = [];
      late dynamic dataRes;
      for(QueryDocumentSnapshot snapshot in snapshotList){
        if(snapshot.id == currentUser.username){
          dataRes = snapshot.data();
        }
      }
      List<String> restaurantIds = dataRes['restaurants'].cast<String>();
      loadingFavoriteRestaurants =false;
      return restaurantIds;
  }
}
