import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DiaryService extends ChangeNotifier {
  String _title = '';
  String _content = '';

  DiaryService() {
    _title = '';
    _content = '';
    loadDiaryFromPreferences();
  }

  void updateTitle(String newTitle) {
    _title = newTitle;
    saveDiaryToPreferences();
    notifyListeners();
  }

  void updateContent(String newContent) {
    _content = newContent;
    notifyListeners();
  }

  String getTitle(){
    return _title; 
  }

  String getContent(){
    return _content; 
  }

  Future<void> saveDiaryToPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('title', _title);
    await prefs.setString('content', _content);
  }

  Future<void> loadDiaryFromPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _title = prefs.getString('title') ?? '';
    _content = prefs.getString('content') ?? '';
    notifyListeners();
  }
}
