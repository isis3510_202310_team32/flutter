import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RatingService extends ChangeNotifier {
  int _stars = 0;
  String _text = '';

  RatingService() {
    _stars = 0;
    _text = '';
    loadRatingFromPreferences();
  }

  void updatestars(int newstars) {
    _stars = newstars;
    saveRatingToPreferences();
    notifyListeners();
  }

  void updateText(String newText) {
    _text = newText;
    notifyListeners();
  }

  int getStars(){
    return _stars; 
  }

  String getText(){
    return _text; 
  }

  Future<void> saveRatingToPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('stars', _stars);
    await prefs.setString('text', _text);
  }

  Future<void> loadRatingFromPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _stars = prefs.getInt('stars') ?? 0;
    _text = prefs.getString('text') ?? '';
    notifyListeners();
  }
}
