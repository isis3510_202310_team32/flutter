import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:intl/intl.dart';

class EventService {
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  Future<void> logSelectContentEvent(List categories) async {
    List<dynamic> dynamicList = categories;
    List<String> stringList =
        dynamicList.map((element) => element.toString()).toList();

    for (var category in categories) {
      await FirebaseAnalytics.instance.logEvent(
        name: 'category_selected',
        parameters: {'category_name': category},
      );
    }
  }

  Future<void> logEventRestaurantPerDate(String pRestaurantName) async {
    var restaurantName = pRestaurantName
        .toLowerCase()
        .replaceAll(' ', '_'); //to make a snakeCase
    var dateFormatter = DateFormat('yyyy-MM-dd');
    var currentDate = dateFormatter.format(DateTime.now());

    await FirebaseAnalytics.instance.logEvent(
      name: "restaurant_selected",
      parameters: {'restaurant_date': '$restaurantName-$currentDate'},
    );
    print('Event sent to FirebaseAnalytics with parameters: '
        '$restaurantName-$currentDate');
  }

  Future<void> sendEventTimeOfLaunch() async {
    DateTime currentDate = DateTime.now().subtract(Duration(hours: 5));
    DateFormat dateFormatter = DateFormat('HH:mm');
    String formattedTime = dateFormatter.format(currentDate);
    print('Current time: $formattedTime');

    await FirebaseAnalytics.instance.logEvent(
      name: "launch_time",
      parameters: {
        'launch_time_detail': formattedTime,
        'launch_time': formattedTime.split(':')[0],
      },
    );
    print('Event sent to FirebaseAnalytics with parameters in launch');
  }

  Future<void> sendEventRestaurantRatingReview(
      String pRestaurantName, int rating) async {
    var restaurantName = pRestaurantName
        .toLowerCase()
        .replaceAll(' ', '_'); //to make a snakeCase

    final DateFormat dateFormatter = DateFormat('yyyy-MM');
    final String formattedDate =
        dateFormatter.format(DateTime.now().subtract(Duration(hours: 5)));

    print('Current time: $formattedDate');

    String qualitative = '<3';
    if (rating == 3) {
      qualitative = '3';
    } else if (rating > 3) {
      qualitative = '>3';
    }
    print('Cualitative: $qualitative');

    await FirebaseAnalytics.instance.logEvent(
      name: "restaurant_rating",
      parameters: {
        'restaurant_review_stars': rating,
        'restaurant_reviewed': restaurantName,
        'restaurant_reviewed_month': formattedDate,
        'restaurant_review_rating': '${restaurantName}_$qualitative'
      },
    );
    print('Event sent to FirebaseAnalytics');
  }

  Future<void> sendEventBudgetConfigurationSaving(double saving) async {
    print('Sending budget saving event');
    await FirebaseAnalytics.instance.logEvent(
      name: "budget_saving",
      parameters: {
        'saving': saving,
      },
    );
  }
}

List<Map<String, String>> getCategoryNameMap(List<String> categories) {
  return categories.map((category) => {'category_name': category}).toList();
}
