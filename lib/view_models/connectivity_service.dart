import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';

class ConnectivityService with ChangeNotifier {
  bool isConnected = false;
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  final Connectivity _connectivity = Connectivity();

  Future<void> initConnectivityStream() async {
    isConnected =
        (await _connectivity.checkConnectivity()) != ConnectivityResult.none;
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      isConnected = result != ConnectivityResult.none;
      notifyListeners();
    });
  }

  void disposeConnectivityStream() {
    _connectivitySubscription.cancel();
  }
}
