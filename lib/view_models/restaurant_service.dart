import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:senefood/models/restaurant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../models/filters.dart';
import '../utils/location.dart';

class RestaurantService with ChangeNotifier {
  List<Restaurant> restaurants = [];
  List<String> restaurantsVisited = [];
  List<Restaurant> favoriteRestaurants = [];       //CAMBIOOOOOOO
  List<String> likesList = [];
  List<String> dislikesList = [];
  List<dynamic> recommendations = [];
  List<Restaurant> restaurantRecommendationsLikes = [];
  List<Restaurant> restaurantRecommendationsDislikes = [];
  Map<String, int> distances = {};
  bool loadingVisited = true;
  bool loadingRestaurants = true;
  bool loadingRecommendations = true;
  bool loadingLike = false;

  late StreamSubscription<Position> positionStream;
  Filters filters = Filters(
      selectedCategories: [],
      sortingSelected: "recommendationScore",
      minPrice: null,
      maxPrice: null,
      name: "");

  final CollectionReference _restaurantsCollection =
      FirebaseFirestore.instance.collection('Restaurants');

  final CollectionReference _recentlyVisitedPerUserCollection =
      FirebaseFirestore.instance.collection('RecentlyVisitedPerUser');

  final CollectionReference _recommendationsPerUserCollection =
      FirebaseFirestore.instance.collection('RecommendationsPerUser');
  
  final CollectionReference _favoriteRestaurantsCollection = 
      FirebaseFirestore.instance.collection("FavouriteRestaurants");      //CAMBIOOOOOO

  Stream<List<QueryDocumentSnapshot>> getRestaurants() {
    return _restaurantsCollection.snapshots().map((snapshot) {
      return snapshot.docs;
    });
  }

  Future<QuerySnapshot<Object?>> getAllRestaurants() async {
    return await _restaurantsCollection.get();
  }

  Future<void> getAllRestaurantsProvider() async {
    if (loadingRestaurants) {
      QuerySnapshot querySnapshot = await _restaurantsCollection.get();
      final source =
          (querySnapshot.metadata.isFromCache) ? "local cache" : "server";
      print("Data fetched from $source}");
      List<QueryDocumentSnapshot> snapshotList = querySnapshot.docs;
      List<Restaurant> res = [];
      for (QueryDocumentSnapshot snapshot in snapshotList) {
        dynamic dataRes = snapshot.data();
        Restaurant newRes = Restaurant(
            id: snapshot.id,
            cleannessScore: dataRes['cleannessScore'],
            stars: dataRes['stars'],
            name: dataRes['name'],
            dishes: dataRes['dishes'],
            recommendationScore: dataRes['recommendationScore'],
            categories: dataRes['categories'],
            coordinates: dataRes['coordinates'],
            reviews: dataRes['reviews'],
            imageName: dataRes['imageName']);
        res.add(newRes);
      }

      restaurants = res;
      try {
        await calculateDistances();
      } catch (_) {}

      loadingRestaurants = false;
      notifyListeners();
    }
  }

  Future<void> getAllFavoriteRestaurantsProvider(String? id) async {
    if (loadingRestaurants) {
      QuerySnapshot querySnapshot = await _favoriteRestaurantsCollection.get();
      final source =
          (querySnapshot.metadata.isFromCache) ? "local cache" : "server";
      print("Data fetched from $source}");
      List<QueryDocumentSnapshot> snapshotList = querySnapshot.docs;
      List<Restaurant> res = [];
      dynamic dataRes;
      for(QueryDocumentSnapshot snapshot in snapshotList){
        if(snapshot.id == id){
          dataRes = snapshot.data();
        }
      }
      List<String> restaurantIds = dataRes['restaurants'];
      Restaurant toAdd;

      for(int i=0; i<restaurantIds.length; i++){
        toAdd = getRestaurant(restaurantIds[i]);
        res.add(toAdd);
      }
      
      favoriteRestaurants = res;
      print("The actual favorite restaurants list is: $favoriteRestaurants");
      
      try {
        await calculateDistances();
      } catch (_) {}

      loadingRestaurants = false;
      notifyListeners();
    }
  }

  Future<DocumentSnapshot> getRecentlyVisitedRestaurants(String uuid) async {
    DocumentSnapshot documentSnapshot =
        await _recentlyVisitedPerUserCollection.doc(uuid).get();
    return documentSnapshot;
  }

  Future<void> getRecentlyVisitedRestaurantsProvider(String uuid) async {
    if (loadingVisited) {
      DocumentSnapshot documentSnapshot =
          await _recentlyVisitedPerUserCollection.doc(uuid).get();
      dynamic data = documentSnapshot.data();
      List<String> newRes = [];
      if (data != null) {
        for (var res in data['recentlyVisitedRestaurants']) {
          newRes.add(res['restaurant']);
        }
        restaurantsVisited = newRes;
      }
      loadingVisited = false;
      notifyListeners();
    }
  }

  Future<DocumentSnapshot> getRecommendedRestaurants(String uuid) async {
    DocumentSnapshot documentSnapshot =
        await _recommendationsPerUserCollection.doc(uuid).get();
    return documentSnapshot;
  }

  Future<void> getRecommendedRestaurantsProvider(String uuid) async {
    if (loadingRecommendations) {
      DocumentSnapshot documentSnapshot =
          await _recommendationsPerUserCollection.doc(uuid).get();
      List<dynamic> newRes = [];
      dynamic data = documentSnapshot.data();
      if (data != null) {
        for (var res in data['recommendations']) {
          newRes.add(res);
        }
        recommendations = newRes;
        List<String> newLikes = [];
        List<String> newDislikes = [];
        for (var rec in recommendations) {
          if (rec["likes"] >= 1) {
            newLikes.add(rec['restaurant']);
          } else if (rec["likes"] <= -1) {
            newDislikes.add(rec['restaurant']);
          }
        }
        likesList = newLikes;
        dislikesList = newDislikes;
      }

      Map computeData = {
        'restaurants': restaurants,
        'recommendations': recommendations,
        'dislikesList': dislikesList,
        'likesList': likesList
      };

      Map computeRes =
          await compute(filterByRecommendationsCompute, computeData);

      restaurantRecommendationsDislikes = computeRes['dislikes'];
      restaurantRecommendationsLikes = computeRes['likes'];
      loadingRecommendations = false;
      notifyListeners();
    }
  }

  Future<void> sendEventRecommendations(int recommendations,
      int likeRecommendations, int dislikedRecommendationss) async {
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    if (recommendations != 0) {
      int finalLikes = likesList.length + likeRecommendations;
      var currentDate = DateTime.now();
      var formattedMonth = DateFormat.MMMM().format(currentDate);
      String month = formattedMonth.toString();
      String spanishMonth = "Mayo";
      print("MONTH: $spanishMonth");

      //First question:
      double dividend = (finalLikes + dislikedRecommendationss).toDouble();
      double divisor = recommendations.toDouble();
      var percentage = ((dividend / divisor) * 100).toInt();
      var roundedPercentage = percentage.round();
      print("THE PERCENTAGE WAS: $roundedPercentage");
      await analytics.logEvent(
          name: "recommendations_rated_distribution",
          parameters: {
            "percentage_distribution": percentage,
            "month_recommendation": spanishMonth
          });

      //Second Question:
      var ratio = "$finalLikes:$dislikedRecommendationss";
      print("THE RATIO IS: $ratio");
      await analytics.logEvent(name: "ratio_rated_frequent", parameters: {
        "ratio_distribution": ratio,
        "month_recommendation": spanishMonth
      });
    }
  }

  Future<void> updateRecommendation(
      String userId, String restaurant, int newValue) async {
    final DocumentReference userRef =
        _recommendationsPerUserCollection.doc(userId);

    final DocumentSnapshot userSnapshot = await userRef.get();
    if (!userSnapshot.exists) {
      // Handle the case where the user document doesn't exist.
      return;
    }

    final dynamic userData = userSnapshot.data()!;
    if (!userData.containsKey('recommendations')) {
      // Handle the case where the recommendations field doesn't exist.
      return;
    }

    final List<dynamic> recommendations = userData['recommendations'];
    for (final recommendation in recommendations) {
      if (recommendation['restaurant'] == restaurant) {
        recommendation['likes'] = newValue;
        break;
      }
    }

    await userRef.update({'recommendations': recommendations});
  }

  Future<void> updateRecommendationScore(
      String restaurantId, int scoreToAdd) async {
    final DocumentReference restaurantRef =
        _restaurantsCollection.doc(restaurantId);

    await restaurantRef
        .update({'recommendationScore': FieldValue.increment(scoreToAdd)});
  }

  void setAttributeFilters(String attributeName, dynamic value) {
    switch (attributeName) {
      case "selectedCategories":
        filters.selectedCategories = value;
        break;
      case "sortingSelected":
        filters.sortingSelected = value;
        break;
      case "minPrice":
        filters.minPrice = value;
        break;
      case "maxPrice":
        filters.maxPrice = value;
        break;
      case "name":
        filters.name = value;
        break;
      default:
        throw ArgumentError("Invalid attribute name: $attributeName");
    }
    notifyListeners();
  }

  void changeLoadingLike(value) {
    loadingLike = value;
    notifyListeners();
  }

  void changeLike(String id, int like) {
    List<String> newLikes = [...likesList];
    List<String> newDislikes = [...dislikesList];
    if (like == 0) {
      newLikes.remove(id);
      newDislikes.remove(id);
    } else if (like >= 1) {
      newLikes.add(id);
      newDislikes.remove(id);
    } else if (like <= -1) {
      newDislikes.add(id);
      newLikes.remove(id);
    }
    likesList = newLikes;
    dislikesList = newDislikes;

    restaurantRecommendationsDislikes =
        filterByRecommendations(restaurants, recommendations, true);
    restaurantRecommendationsLikes =
        filterByRecommendations(restaurants, recommendations, false);

    notifyListeners();
  }

  void changeList(likes, dislikes) {
    likesList = likes;
    dislikesList = dislikes;
    notifyListeners();
  }

  int getLikeNumber(restaurant) {
    dynamic res = restaurant;
    dynamic likes = likesList;
    dynamic dislikes = dislikesList;

    try {
      if (likes.contains(res.id)) {
        return 1;
      } else if (dislikes.contains(res.id)) {
        return -1;
      } else {
        return 0;
      }
    } catch (e) {
      if (likes.contains(res.id)) {
        return 1;
      } else if (dislikes.contains(res.id)) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  dynamic calculateDistances() async {
    Position location = await getCurrentLocation();
    updateDistances(location);
  }

  void updateDistances(Position position) {
    for (dynamic res in restaurants) {
      distances[res.id] = distanceFromLocation(res, position);
    }
    notifyListeners();
  }

  dynamic filterAndSortRestaurants(List<dynamic> restaurants) {
    if (filters.name != "") {
      print(filters.name);
      restaurants = restaurants.where((restaurant) {
        return restaurant.name.contains(filters.name);
      }).toList();
    }

    // Filter out restaurants based on category
    if (filters.selectedCategories.isNotEmpty) {
      restaurants = restaurants.where((restaurant) {
        for (var category in restaurant.categories) {
          if (filters.selectedCategories.contains(category)) {
            return true;
          }
        }
        return false;
      }).toList();
    }

    // Filter out restaurants based on price range
    if (filters.minPrice != null || filters.maxPrice != null) {
      restaurants = restaurants.where((restaurant) {
        for (var dish in restaurant.dishes) {
          if (dish['price'] == null) {
            return false;
          }
          if (filters.minPrice != null && filters.maxPrice != null) {
            if (dish['price'] < filters.minPrice ||
                dish['price'] > filters.maxPrice) {
              return false;
            }
          } else if (filters.minPrice == null &&
              filters.maxPrice != null &&
              dish['price'] > filters.maxPrice) {
            return false;
          } else if (filters.minPrice != null &&
              filters.maxPrice == null &&
              dish['price'] < filters.minPrice) {
            return false;
          }
        }

        return true;
      }).toList();
    }
    dynamic averageValue(restaurant, attribute) {
      num sum = 0;
      num count = 0;
      /* for (var dish in restaurant.dishes) {
        sum += dish[attribute];
        count++;
      }
      */
      var dish;
      for (int i = 0; i < restaurant.dishes.length; i++) {
        dish = restaurant.dishes[i];
        sum += dish[attribute];
        count++;
      }

      return sum / count;
    }

    // Sort restaurants based on sortingSelected
    switch (filters.sortingSelected) {
      case 'price':
        restaurants.sort((a, b) =>
            averageValue(a, 'price').compareTo(averageValue(b, 'price')));
        break;
      case 'recommendationScore':
        restaurants.sort(
            (a, b) => b.recommendationScore.compareTo(a.recommendationScore));
        break;
      case 'distance':
        restaurants
            .sort((a, b) => compareDistances(distances[a.id], distances[b.id]));
        break;
      case 'time':
        restaurants.sort((a, b) => averageValue(a, 'preparationTime')
            .compareTo(averageValue(b, 'preparationTime')));
        break;
      case 'cleannessScore':
        restaurants
            .sort((a, b) => a.cleannessScore.compareTo(b.cleannessScore));
        break;
    }
    return restaurants;
  }

  dynamic compareDistances(distanceA, distanceB) {
    return distanceA.compareTo(distanceB);
  }

  dynamic filterAndSortByLocation(List<dynamic> restaurants) {
    restaurants = restaurants
        .where((res) => (distances[res.id] ?? 10000000) < 100)
        .toList();

    restaurants
        .sort((a, b) => compareDistances(distances[a.id], distances[b.id]));
    return restaurants;
  }

  dynamic filterByVisited(List<dynamic> restaurants, List<String> visited) {
    if (visited.isNotEmpty) {
      restaurants = restaurants.where((restaurant) {
        if (visited.contains(restaurant.id)) {
          return true;
        }
        return false;
      }).toList();
    }
    return restaurants;
  }

  List<Restaurant> filterByRecommendations(List<Restaurant> restaurants,
      List<dynamic> recommendations, bool dislikes) {
    List<Restaurant> newRes = [];

    List<String> newLikes = [...likesList];
    List<String> newDislikes = [...dislikesList];
    if (recommendations.isNotEmpty) {
      /*
      restaurants.forEach((restaurant) {
        for (var rec in recommendations) {
          int curLike = newLikes.contains(restaurant.id)
              ? 1
              : newDislikes.contains(restaurant.id)
                  ? -1
                  : 0;
          if (dislikes) {
            if (rec['restaurant'] == restaurant.id && curLike <= -1) {
              newRes.add(restaurant);
            }
          } else {
            if (rec['restaurant'] == restaurant.id && curLike >= 0) {
              newRes.add(restaurant);
            }
          }
        }
      }*/
      for (int i = 0; i < restaurants.length; i++) {
        var restaurant = restaurants[i];
        for (var j = 0; j < recommendations.length; j++) {
          var rec = recommendations[j];
          int curLike = newLikes.contains(restaurant.id)
              ? 1
              : newDislikes.contains(restaurant.id)
                  ? -1
                  : 0;
          if (dislikes) {
            if (rec['restaurant'] == restaurant.id && curLike <= -1) {
              newRes.add(restaurant);
            }
          } else {
            if (rec['restaurant'] == restaurant.id && curLike >= 0) {
              newRes.add(restaurant);
            }
          }
        }
      }
    }
    newRes
        .sort((a, b) => b.recommendationScore.compareTo(a.recommendationScore));
    return newRes;
  }

  List<String> getCategories(restaurants) {
    List<String> categories = [];
    for (var restaurant in restaurants) {
      for (var category in restaurant.categories) {
        categories.add(category);
      }
    }
    return categories;
  }

  Future<void> addReview(
      String restaurantId, String? user, int stars, String text) async {
    final CollectionReference restaurantsCollection =
        FirebaseFirestore.instance.collection('Restaurants');
    final DocumentReference restaurantRef =
        restaurantsCollection.doc(restaurantId);

    // Get the current reviews of a restaurant based on the id
    final DocumentSnapshot restaurantSnapshot = await restaurantRef.get();
    final Map<String, dynamic> data =
        restaurantSnapshot.data() as Map<String, dynamic>;
    final List<dynamic> currentReviews = data['reviews'];

    final uuid = Uuid();
    final reviewId = uuid.v4();

    // Add the new review
    final Map<String, dynamic> newReview = {
      'id': reviewId,
      'stars': stars,
      'text': text,
      'user': user,
    };
    final List<dynamic> newReviews = List.from(currentReviews)..add(newReview);

    // Update the reviews of the restaurant in the Firestore database
    await restaurantRef.update({'reviews': newReviews});
    loadingRestaurants = true;
/*
  final DocumentSnapshot restaurantSnapshotUpdated = await restaurantRef.get();
  final Map<String, dynamic> dataUpdated = restaurantSnapshotUpdated.data() as Map<String, dynamic>;
  final List<dynamic> reviewsUpdated = dataUpdated['reviews'];
  */
    //  getAllRestaurantsProvider();
    // notifyListeners();
  }

  Restaurant getRestaurant(String id) {
    return restaurants.firstWhere((Restaurant res) => res.id == id);
  }

  Future<double> recalculateGlobalScore(
      String id, int stars, String text) async {
    var json = jsonEncode({'text': text, 'stars': stars});

    try {
      // POST request
      var url = Uri.parse(
          'https://reviews-engine.onrender.com/calculate_review_score');
      var response = await http.post(
        url,
        headers: {'Content-Type': 'application/json'},
        body: json,
      );

      if (response.statusCode == 200) {
        var responseJson = jsonDecode(response.body);
        if (responseJson.containsKey('review_score')) {
          var score = responseJson['review_score'];
          getRestaurant(id).recommendationScore += score;
          var formattedScore = double.parse(
              getRestaurant(id).recommendationScore.toStringAsFixed(2));

          final CollectionReference restaurantsCollection =
              FirebaseFirestore.instance.collection('Restaurants');
          final DocumentReference restaurantRef =
              restaurantsCollection.doc(id);
          await restaurantRef.update({'recommendationScore': formattedScore});

          notifyListeners();
          return score;
        } else if (responseJson.containsKey('error')) {
          var error = responseJson['error'];
          throw Exception(error);
        } else {
          throw Exception('Could not parse review score from response');
        }
      } else {
        throw Exception('Error: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }

  void initPositionStream() {
    positionStream = Geolocator.getPositionStream(
            intervalDuration: Duration(seconds: 2),
            desiredAccuracy: LocationAccuracy.bestForNavigation)
        .listen((Position? position) {
      if (position != null) {
        updateDistances(position);
      }
    });
  }

  void disposePositionStream() {
    positionStream.cancel();
  }

  Future<String> isolateExample() async {
    String testFunction(Map data) {
      return "${data['name']} having ${data['number']}";
    }

    return await compute(testFunction, {'name': "Hello World", 'number': 1});
  }

  Future<void> saveFiltersToPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (filters.minPrice != null) {
      await prefs.setDouble('minPrice', filters.minPrice);
    } else {
      await prefs.remove('minPrice');
    }
    if (filters.maxPrice != null) {
      await prefs.setDouble('maxPrice', filters.maxPrice);
    } else {
      await prefs.remove('maxPrice');
    }
    await prefs.setString('name', filters.name);
    await prefs.setString('sortingSelected', filters.sortingSelected);
    await prefs.setString(
        'selectedCategories', filters.selectedCategories.join('-'));
  }

  Future<void> loadFiltersFromPreferences() async {
    SharedPreferences.getInstance().then((prefs) {
      filters.minPrice = prefs.getDouble('minPrice');
      filters.maxPrice = prefs.getDouble('maxPrice');
      filters.name = prefs.getString('name') ?? '';
      filters.sortingSelected =
          prefs.getString('sortingSelected') ?? 'recommendationScore';
      filters.selectedCategories = prefs.getString('selectedCategories') == ''
          ? []
          : prefs.getString('selectedCategories')?.split('-') ?? [];
      notifyListeners();
    });
  }

  List<dynamic> selectDishesBudget(
      List<String> preferences, List<String> days, int budget) {
    List<dynamic> dishesModified = [];
    for (var res in restaurants) {
      for (var dish in res.dishes) {
        Map<String, dynamic> newDish = {
          'name': dish['name'],
          'price': dish['price'],
          'image': dish['imageName'],
          'restaurant': res.name,
          'categories': res.categories.join(','),
        };
        dishesModified.add(newDish);
      }
    }
    int compareDishes(a, b) {
      bool matchA = false;
      bool matchB = false;
      a['categories'].split(',').forEach((cat) {
        if (preferences.contains(cat)) {
          matchA = true;
        }
      });
      b['categories'].split(',').forEach((cat) {
        if (preferences.contains(cat)) {
          matchB = true;
        }
      });

      if (matchA && matchB) {
        return a['price'] > b['price']
            ? 1
            : a['price'] < b['price']
                ? -1
                : 0;
      } else if (matchA) {
        return -1;
      } else if (matchB) {
        return 1;
      } else {
        return a['price'] > b['price']
            ? 1
            : a['price'] < b['price']
                ? -1
                : 0;
      }
    }

    dishesModified.sort(compareDishes);
    List<dynamic> daysDishes = dishesModified.getRange(0, days.length).toList();
    daysDishes.shuffle();
    num sumBudget = 0;
    int count = 0;
    for (var dish in daysDishes) {
      sumBudget += dish['price'];
      dish['day'] = days[count];
      count++;
    }
    if (sumBudget < budget) {
      return daysDishes;
    } else {
      return [];
    }
  }
}

Map filterByRecommendationsCompute(Map data) {
  List<Restaurant> restaurants = data['restaurants'];
  List<dynamic> recommendations = data['recommendations'];
  List<String> likesList = data['likesList'];
  List<String> dislikesList = data['dislikesList'];
  List<Restaurant> likesRes = [];
  List<Restaurant> dislikesRes = [];

  if (recommendations.isNotEmpty) {
    for (var restaurant in restaurants) {
      for (var rec in recommendations) {
        int curLike = likesList.contains(restaurant.id)
            ? 1
            : dislikesList.contains(restaurant.id)
                ? -1
                : 0;
        if (rec['restaurant'] == restaurant.id && curLike <= -1) {
          dislikesRes.add(restaurant);
        }
        if (rec['restaurant'] == restaurant.id && curLike >= 0) {
          likesRes.add(restaurant);
        }
      }
    }
  }
  dislikesRes
      .sort((a, b) => b.recommendationScore.compareTo(a.recommendationScore));

  likesRes
      .sort((a, b) => b.recommendationScore.compareTo(a.recommendationScore));

  Map response = {'dislikes': dislikesRes, 'likes': likesRes};

  return response;
}
