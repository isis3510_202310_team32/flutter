import 'package:flutter/cupertino.dart';

class StackService with ChangeNotifier {
  int routeIndex = 5;
  int tabIndex = -1;

  void onItemTapped(int tab, int route) {
    routeIndex = route;
    tabIndex = tab;
    notifyListeners();
  }

  void onTabChange(int index) {
    tabIndex = index;
    routeIndex = index;
    notifyListeners();
  }

  bool showNavigationBar() {
    return tabIndex != -1;
  }
}
