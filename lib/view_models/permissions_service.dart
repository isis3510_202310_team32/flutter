import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart' as glc;
import 'package:permission_handler/permission_handler.dart';

class PermissionsService with ChangeNotifier {
  late bool locationPermission;
  late StreamSubscription<ServiceStatus> locationStream;
  Future<void> checkLocationPermission() async {
    PermissionStatus status = await Permission.location.status;
    ServiceStatus serviceStatus = await Permission.location.serviceStatus;
    if (status == PermissionStatus.granted &&
        serviceStatus == ServiceStatus.enabled) {
      locationPermission = true;
    } else {
      locationPermission = false;
    }
    notifyListeners();
  }

  void initLocationStream(Function calculateDistances) {
    glc.Geolocator.getServiceStatusStream()
        .listen((glc.ServiceStatus status) async {
      print(status);
      if (status == glc.ServiceStatus.enabled) {
        locationPermission = true;
        await calculateDistances();
      } else {
        locationPermission = false;
      }
      notifyListeners();
    });
  }
}
