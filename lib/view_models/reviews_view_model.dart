import 'package:flutter/material.dart';
import 'package:senefood/models/review.dart'; 

class ReviewsViewModel extends ChangeNotifier {
  List<Review> _reviews = [];
  List<Review> get reviews => _reviews;

  set reviews(List<Review> value) {
    _reviews = value;
    notifyListeners();
  }
}