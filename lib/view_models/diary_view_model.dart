import 'package:flutter/material.dart';
import 'package:senefood/models/diary_entry.dart'; 



import '../databases/diary_database.dart';
import './../dao/DiaryDao.dart'; 

class DiaryViewModel with ChangeNotifier {
  List<DiaryEntry> _diaryEntries = [];
  late DiaryDao _diaryDao;

  List<DiaryEntry> get diaryEntries => _diaryEntries;

  Future<void> initialize() async {
    final database = await $FloorDiaryDatabase.databaseBuilder('diary_database.db').build();

    _diaryDao = database.diaryDao;
    await fetchDiaryEntries();
  }
Future<void> fetchDiaryEntries() async {
    if (_diaryDao == null) {
      print("estoy esperando que se inicialize en fetch"); 
      await initialize();
    }
    _diaryEntries = await _diaryDao.getAllEntries();
    notifyListeners();
  }

  Future<void> saveDiaryEntry(String title, String text, DateTime date, String restaurantId, String restaurantName) async {
     if (_diaryDao == null) {
      print("estoy esperando que se inicialize en save"); 
      await initialize();
    }

    final entry = DiaryEntry(
      null,
      title,
      text,
      date.millisecondsSinceEpoch,
      restaurantId,
      restaurantName,
    );

    await _diaryDao.insertEntry(entry);
    _diaryEntries = await _diaryDao.getAllEntries();
    await fetchDiaryEntries();
    notifyListeners();
  }
}
