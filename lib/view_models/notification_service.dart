import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class LocalNotificationService {
  //Singleton
  LocalNotificationService._privateConstructor();

  static final LocalNotificationService _instance =
      LocalNotificationService._privateConstructor();

  //Factory
  factory LocalNotificationService() {
    return _instance;
  }

  final _localNotificationService = FlutterLocalNotificationsPlugin();

  Future<void> initialize() async {
    tz.initializeTimeZones();
    const AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings('@drawable/alarm');

    final InitializationSettings settings =
        InitializationSettings(android: androidInitializationSettings);

    await _localNotificationService.initialize(
      settings,
      onSelectNotification: onSelectNotification,
    );
  }

  Future<NotificationDetails> _notificationDetails() async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('channel_id', 'channel_name',
            channelDescription: 'description',
            importance: Importance.max,
            priority: Priority.max);

    return const NotificationDetails(android: androidNotificationDetails);
  }

  Future<void> showNotification({
    required int id,
    required String title,
    required String body,
  }) async {
    final details = await _notificationDetails();
    await _localNotificationService.show(id, title, body, details);
  }

  Future<void> showScheduledNotification({
    required int id,
    required String title,
    required String body,
  }) async {
    final details = await _notificationDetails();

    final now = tz.TZDateTime.now(tz.local);

    tz.TZDateTime _nextInstanceOfZeroAM() {
      final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
      tz.TZDateTime scheduledDate =
          tz.TZDateTime(tz.local, now.year, now.month, now.day, 5);
      if (scheduledDate.isBefore(now)) {
        scheduledDate = scheduledDate.add(const Duration(days: 1));
      }
      return scheduledDate;
    }

    tz.TZDateTime _nextInstanceOfZundayZeroAM() {
      tz.TZDateTime scheduledDate = _nextInstanceOfZeroAM();
      while (scheduledDate.weekday != DateTime.sunday) {
        scheduledDate = scheduledDate.add(const Duration(days: 1));
      }
      return scheduledDate;
    }

    var scheduledDate = _nextInstanceOfZundayZeroAM();

    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(Duration(days: 7));
    }
    print('Scheduled date: $scheduledDate');
    var nowDate = tz.TZDateTime(
        tz.local, now.year, now.month, now.day, now.hour, now.minute);
    print('now date: $nowDate');

    await _localNotificationService.zonedSchedule(
      id,
      title,
      body,
      scheduledDate,
      details,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  Future<void> showBudgetScheduledNotification({
    required int id,
    required String title,
    required String body,
  }) async {
    final details = await _notificationDetails();

    // Set the notification time to 12:20 pm every day. There is a problem with the time zone and it is 5 hours ahead.
    final now = tz.TZDateTime.now(tz.local);
    var scheduledDate =
        tz.TZDateTime(tz.local, now.year, now.month, now.day, 17, 20);

    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(Duration(days: 1));
    }
    print('Scheduled date: $scheduledDate');
    var nowDate = tz.TZDateTime(
        tz.local, now.year, now.month, now.day, now.hour, now.minute);
    print('now date: $nowDate');

    await _localNotificationService.zonedSchedule(
      id,
      title,
      body,
      scheduledDate,
      details,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  void onSelectNotification(String? payload) {
    print('payload $payload');
  }
}
