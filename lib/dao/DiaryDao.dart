import 'package:floor/floor.dart';

import './../models/diary_entry.dart'; 

@dao
abstract class DiaryDao {
  @Query('SELECT * FROM diary')
  Future<List<DiaryEntry>> getAllEntries();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertEntry(DiaryEntry entry);
}