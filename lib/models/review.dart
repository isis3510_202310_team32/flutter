class Review{
  String id; 
  String user;
  int stars;
  String text; 
  
  Review({required this.id, required this.user, required this.stars, required this.text});

}