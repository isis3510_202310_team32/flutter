class AuthUser{
  String? id;
  String? username;
  String? displayName;
  List<String>? preferences;
  List<String>? favoriteRestaurantsIds = [];

  
  AuthUser({this.id, this.displayName, this.username, this.preferences, this.favoriteRestaurantsIds});

  String? get getId{
    return id;
  }

  String? get getDisplayName{
    return displayName;
  }

  String? get getUserName{
    return username;
  }

  List<String>? get getPreferences{
    return preferences;
  }

  List<String>? get getFavoriteRestaurants{
    return favoriteRestaurantsIds;
  }

  void setPreferences(List<String>? newPrefs){
    preferences = newPrefs;
  }

  void setFavoriteRestaurants(List<String>? newRests){
    favoriteRestaurantsIds = newRests;
  }

  void addPreferenceToList(String preference){
    preferences?.add(preference);
  }

  void addRestaurantToFavorites(String restaurantId){
    favoriteRestaurantsIds?.add(restaurantId);
  }

  void removeRestaurantFromFavorites(String restaurantId){
    favoriteRestaurantsIds?.remove(restaurantId);
  }




  

}