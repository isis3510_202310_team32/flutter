class Dish{
  String id;
  String imageName;
  String name; 
  int preparationTime; 
  double price; 
  int stars; 

  
  Dish({required this.id, required this.name, required this.imageName, required this.preparationTime, required this.price, required this.stars});

}