import 'package:cloud_firestore/cloud_firestore.dart';

class Restaurant {
  String id;
  String name;
  num cleannessScore;
  String imageName;
  num stars;
  num recommendationScore;
  List<dynamic> categories;
  List<dynamic> dishes;
  List<dynamic> reviews;
  GeoPoint coordinates;

  Restaurant(
      {required this.cleannessScore,
      required this.stars,
      required this.name,
      required this.dishes,
      required this.recommendationScore,
      required this.categories,
      required this.coordinates,
      required this.reviews,
      required this.imageName,
      required this.id});

  // factory Restaurant.fromJson(dynamic json) {
  //   return Restaurant(
  //       name: json['name'] as dynamic,
  //       cleannessScore: json.cleannessScore as dynamic,
  //       stars: json.stars as dynamic,
  //       dishes: json.dishes as dynamic,
  //       recommendationScore: json['recommendationScore'] as dynamic,
  //       categories: json.categories as dynamic,
  //       coordinates: json.coordinates as dynamic,
  //       reviews: json['reviews'] as dynamic,
  //       imageName: json['imageName'] as dynamic);
  // }
}
