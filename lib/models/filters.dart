import 'package:flutter/cupertino.dart';

class Filters with ChangeNotifier {
  List<String> selectedCategories;
  String sortingSelected;
  dynamic minPrice;
  dynamic maxPrice;
  String name;

  Filters(
      {required this.selectedCategories,
      required this.sortingSelected,
      required this.minPrice,
      required this.maxPrice,
      required this.name});
}
