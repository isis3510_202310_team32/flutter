class SortingOption {
  String name;
  String value;

  SortingOption({required this.name, required this.value});
}
