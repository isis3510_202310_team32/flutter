import 'package:floor/floor.dart';

@Entity(tableName: 'diary')
class DiaryEntry {
  @PrimaryKey(autoGenerate: true)
  final int? id;

  final String title;
  final String content;

  @ColumnInfo(name: 'date')
  final int timestamp;

  final String restaurantId;
  final String restaurantName;

  DiaryEntry(this.id, this.title, this.content, this.timestamp, this.restaurantId, this.restaurantName);

  DateTime get date => DateTime.fromMillisecondsSinceEpoch(timestamp);

}
