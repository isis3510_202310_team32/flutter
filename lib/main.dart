import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/database_service.dart';
import 'package:senefood/view_models/diary_preferences_service.dart';
import 'package:senefood/view_models/diary_view_model.dart';
import 'package:senefood/view_models/event_service.dart';
import 'package:senefood/view_models/permissions_service.dart';
import 'package:senefood/view_models/rating_service.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/view_models/stack_service.dart';
import 'package:senefood/view_models/user_service.dart';
import 'package:senefood/views/screens/BudgetView.dart';
import 'package:senefood/views/screens/BudgetView2.dart';
import 'package:senefood/views/screens/budget_list.dart';
import 'package:senefood/views/screens/favorite_restaurants_view.dart';
import 'package:senefood/views/screens/general_login_view.dart';
import 'package:senefood/views/screens/location_list.dart';
import 'package:senefood/views/screens/map_view.dart';
import 'package:senefood/views/screens/preferencesView2.dart';
import 'package:senefood/views/screens/profile_view.dart';
import 'package:senefood/views/screens/recommend_list.dart';
import 'package:senefood/views/screens/restaurants_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'firebase_options.dart';
import 'view_models/notification_service.dart';
import 'views/screens/recommendView.dart';
import 'views/widgets/navigation_bar.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalNotificationService().initialize();
  await LocalNotificationService().showScheduledNotification(
    id: 0,
    title: 'Its lunch time!',
    body: 'Come get your own recommendations',
  );

  SharedPreferences prefs = await SharedPreferences.getInstance();
  await LocalNotificationService().showBudgetScheduledNotification(
    id: 1,
    title: 'You saved ${prefs.getInt('budget') ?? 0}',
    body: 'Configure your budger for the next week!',
  );

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FirebaseFirestore.instance.settings = const Settings(
    persistenceEnabled: true,
    cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED,
  );

  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => RestaurantService()),
          ChangeNotifierProvider(create: (_) => StackService()),
          ChangeNotifierProvider(create: (_) => AuthUserService()),
          ChangeNotifierProvider(create: (_) => ConnectivityService()),
          ChangeNotifierProvider(create: (_) => RatingService()),
          ChangeNotifierProvider(create: (_) => PermissionsService()),
          ChangeNotifierProvider(create: (_) => DatabaseService()),
          ChangeNotifierProvider<DiaryViewModel>(
              create: (_) => DiaryViewModel(), lazy: false),
          ChangeNotifierProvider(create: (_) => DiaryService()),

        ],
        builder: (context, child) {
          return MaterialApp(
              title: 'My App',
              theme: ThemeData(
                  primaryColor: Color(0xFFFE6454),
                  splashColor: Color(0xFFB9F6D9),
                  hintColor: Color(0x33787880),
                  disabledColor: Color(0xFFe3e3e3)),
              home: ScreensContainer());
        });
  }
}

class ScreensContainer extends StatefulWidget {
  const ScreensContainer({Key? key}) : super(key: key);

  @override
  State<ScreensContainer> createState() => _ScreensContainerState();
}

class _ScreensContainerState extends State<ScreensContainer> {
  late ConnectivityService connectivityService;
  late PermissionsService pemissionsService;
  late RestaurantService restaurantService;
  late DiaryViewModel diaryService;

  @override
  void initState() {
    final EventService eventService = EventService();
    eventService.sendEventTimeOfLaunch();

    connectivityService =
        Provider.of<ConnectivityService>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await connectivityService.initConnectivityStream();
    });

    pemissionsService = Provider.of<PermissionsService>(context, listen: false);
    restaurantService = Provider.of<RestaurantService>(context, listen: false);
    diaryService = Provider.of<DiaryViewModel>(context, listen: false);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await pemissionsService.checkLocationPermission();
      pemissionsService
          .initLocationStream(restaurantService.calculateDistances);
    });
    Provider.of<DatabaseService>(context, listen: false).init();
    super.initState();
  }

  @override
  void dispose() {
    connectivityService.disposeConnectivityStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> screens = [
      RestaurantsView(),
      BudgetView2(),
      RecommendView(),
      UserProfile(),
      RecommendList(),
      LoginView(),
      MapView(),
      PreferencesView2(),
      LocationList(),
      BudgetView(),
      BudgetList(),
      FavoriteRestaurantsView()
    ];

    int routeIndex = Provider.of<StackService>(context).routeIndex;
    List<Widget> containerList = [];
    for (int i = 0; i < screens.length; i++) {
      containerList.add(
        Container(),
      );
    }
    containerList[routeIndex] = screens[routeIndex];
    return Scaffold(
      body: IndexedStack(
        index: routeIndex,
        children: containerList,
      ),
      bottomNavigationBar:
          Provider.of<StackService>(context).showNavigationBar()
              ? BottomNavigation()
              : SizedBox(height: 0, width: 0),
    );
  }
}


