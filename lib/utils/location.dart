import 'package:geolocator/geolocator.dart';

Future<Position> getCurrentLocation() async {
  return await Geolocator.getCurrentPosition(
    desiredAccuracy: LocationAccuracy.high,
  );
}

int _calculateDistance(double startLatitude, double startLongitude,
    double endLatitude, double endLongitude) {
  double distanceInMeters = Geolocator.distanceBetween(
      startLatitude, startLongitude, endLatitude, endLongitude);
  return distanceInMeters.round();
}

dynamic distanceFromActualLocation(restaurant) async {
  Position currentPosition = await getCurrentLocation();
  double currentLatitude = currentPosition.latitude;
  double currentLongitude = currentPosition.longitude;

  // Calculate the distance between two sets of coordinates
  return _calculateDistance(currentLatitude, currentLongitude,
      restaurant.coordinates.latitude, restaurant.coordinates.longitude);
}

dynamic distanceFromLocation(restaurant, location) {
  double currentLatitude = location.latitude;
  double currentLongitude = location.longitude;

  // Calculate the distance between two sets of coordinates
  return _calculateDistance(currentLatitude, currentLongitude,
      restaurant.coordinates.latitude, restaurant.coordinates.longitude);
}
