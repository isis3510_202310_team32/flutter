import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/reviews_view_model.dart';
import 'package:senefood/views/screens/reviews__create.dart';
import 'package:senefood/views/screens/reviews_row.dart';

import '../../models/restaurant.dart';
import '../../view_models/connectivity_service.dart';
import '../../view_models/restaurant_service.dart';

class ReviewsView extends StatefulWidget {
  final ReviewsViewModel reviewsViewModel;
  final String restaurantId;

  const ReviewsView({
    Key? key,
    required this.reviewsViewModel,
    required this.restaurantId,
  }) : super(key: key);

  @override
  _ReviewsViewState createState() => _ReviewsViewState();
}

class _ReviewsViewState extends State<ReviewsView> {
  late ReviewsViewModel _reviewsViewModel;

  @override
  void initState() {
    super.initState();
    _reviewsViewModel = widget.reviewsViewModel;
  }

  @override
  Widget build(BuildContext context) {
    Restaurant restaurant = Provider.of<RestaurantService>(context)
        .getRestaurant(widget.restaurantId);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Reviews',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Theme.of(context).primaryColor),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ReviewsCreate(
                            reviewsViewModel: _reviewsViewModel,
                            restaurant: restaurant,
                          )));
            },
          ),
        ],
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: ListView(
              children: restaurant.reviews.map((review) {
                return ReviewsRow(
                  review: review,
                  reviewsViewModel: _reviewsViewModel,
                );
              }).toList(),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: !Provider.of<ConnectivityService>(context).isConnected
                ? Container(
                    color: Color(0xFFFE6454),
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: Text(
                          'Senefood offline',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
