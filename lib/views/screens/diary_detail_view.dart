import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:senefood/models/diary_entry.dart';
import 'package:senefood/view_models/diary_view_model.dart';
import 'package:senefood/views/widgets/image_card_diary.dart';

import '../../view_models/restaurant_service.dart';

class DiaryDetailView extends StatelessWidget {
  final DiaryEntry entry;

  const DiaryDetailView({required this.entry});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Diary Entry',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              entry.title,
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Text(
              'Restaurant: ${entry.restaurantName}',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Center(child: ImageCardDiary(imageUrl: restaurantUrl(context))),
            SizedBox(height: 8),
            Text(
              entry.content,
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(height: 8),
            Text(
              ' ${DateFormat('yyyy-MM-dd').format(entry.date)}',
              style: TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }

  String restaurantUrl(context) {
    final id = this.entry.restaurantId; 
    final restaurant = Provider.of<RestaurantService>(context, listen: false).getRestaurant(id); 
    return restaurant.imageName; 
  }
}
