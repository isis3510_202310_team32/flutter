import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../view_models/stack_service.dart';

class BudgetPendingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Budget',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Text('Have you set up your weekly budget?',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    textAlign: TextAlign.center),
                SizedBox(
                  height: 30,
                ),
                Text(
                    'Define your budget, savings percentage and the days of the week you want to save',
                    style: TextStyle(fontSize: 15)),
                SizedBox(height: 20),
                Text(
                    'This way Senefood will be able to recommend the best dishes just for you!',
                    style: TextStyle(fontSize: 15)),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text('(the budget resets every sunday)',
                        style: TextStyle(fontSize: 15)),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<StackService>(context, listen: false)
                        .onItemTapped(1, 9);
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).splashColor),
                    padding: MaterialStateProperty.all<EdgeInsets>(
                      EdgeInsets.only(left: 27, right: 27, top: 17, bottom: 17),
                    ),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14),
                      ),
                    ),
                  ),
                  child: Text(
                    'Configure Budget',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
