import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/permissions_service.dart';
import 'package:senefood/views/screens/restaurant_list.dart';
import 'package:senefood/views/widgets/unavailable_dialog.dart';

import '../../view_models/restaurant_service.dart';
import '../../view_models/stack_service.dart';
import '../widgets/recommend_button.dart';

class RecommendView extends StatefulWidget {
  @override
  State<RecommendView> createState() => _RecommendViewState();
}

class _RecommendViewState extends State<RecommendView> {
  late PermissionsService permissionsService;
  late RestaurantService restaurantService;

  @override
  void initState() {
    permissionsService =
        Provider.of<PermissionsService>(context, listen: false);
    restaurantService = Provider.of<RestaurantService>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await permissionsService.checkLocationPermission();
      permissionsService
          .initLocationStream(restaurantService.calculateDistances);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ConnectivityService connectivityService =
        Provider.of<ConnectivityService>(context);
    PermissionsService permissionsService =
        Provider.of<PermissionsService>(context);
    return Material(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text(
            'Recommend',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    RecommendButton(
                        icon: Icons.location_on,
                        title: 'Location',
                        disabled: !connectivityService.isConnected ||
                            !permissionsService.locationPermission,
                        onPressed: () {
                          Provider.of<StackService>(context, listen: false)
                              .onItemTapped(2, 8);
                        }),
                    RecommendButton(
                        icon: Icons.favorite,
                        title: 'Preferences',
                        disabled: !connectivityService.isConnected,
                        onPressed: () {
                          Provider.of<StackService>(context, listen: false)
                              .onItemTapped(2, 4);
                        }),
                  ],
                ),
                Row(
                  children: [
                    RecommendButton(
                        icon: Icons.group,
                        title: 'Group \npreferences',
                        disabled: !connectivityService.isConnected,
                        onPressed: () {}),
                    RecommendButton(
                        icon: Icons.star,
                        title: 'Favorites',
                        disabled: !connectivityService.isConnected,
                        onPressed: () {
                          Provider.of<StackService>(context, listen: false)
                              .onItemTapped(2, 11);
                        }),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: ElevatedButton(
                          onPressed: () {
                            if (!connectivityService.isConnected) {
                              showUnavailableDialog(context);
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor: connectivityService.isConnected
                                ? MaterialStateProperty.all<Color>(
                                    Theme.of(context).primaryColor)
                                : MaterialStateProperty.all<Color>(
                                    Theme.of(context).disabledColor),
                            padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.only(bottom: 15, top: 15),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                          ),
                          child: Text(
                            'Wish me luck',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    'Recommended restaurants',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
                RestaurantList(
                  isFiltered: true,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
