import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/diary_preferences_service.dart';

import '../../models/restaurant.dart';
import '../../view_models/connectivity_service.dart';
import '../../view_models/restaurant_service.dart';
import '../../view_models/diary_view_model.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'dart:isolate';

class DiaryCreate extends StatefulWidget {
  final DiaryViewModel diaryViewModel;

  const DiaryCreate({
    Key? key,
    required this.diaryViewModel,
  }) : super(key: key);

  @override
  _DiaryCreateState createState() => _DiaryCreateState();
}

class _DiaryCreateState extends State<DiaryCreate> {
  String title = '';
  String content = '';
  String restaurantId = '';
  bool isConnected = true;
  bool isDataSaved = false;

  Restaurant? selectedRestaurant;
  List<Restaurant> restaurants = [];

  bool isRestaurantSelected = false;

  final TextEditingController _controller = TextEditingController();
  final TextEditingController _controller2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    initValues();
  }

  void initValues() async {
    final diaryViewModel = Provider.of<DiaryViewModel>(context, listen: false);

    final restaurantService =
        Provider.of<RestaurantService>(context, listen: false);
    await restaurantService.getAllRestaurantsProvider();

    bool isLoading = true;

    setState(() {
      restaurants = restaurantService.restaurants;
    });

    final diaryService = Provider.of<DiaryService>(context, listen: false);
    diaryService.loadDiaryFromPreferences;

    final savedTitle = diaryService.getTitle();
    final savedContent = diaryService.getContent();

    setState(() {
      restaurants = restaurantService.restaurants;
      title = savedTitle;
      content = savedContent;
      _controller.text = savedTitle;
      _controller2.text = savedContent;
      isDataSaved = savedTitle.isNotEmpty && savedContent.isNotEmpty;

      print("ENTRE ACA: " + title);
    });
  }

  void submitDiary() async {
    isConnected =
        Provider.of<ConnectivityService>(context, listen: false).isConnected;
    if (!isConnected) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Connectivity'),
            content:
                Text('Please check your internet connection and try again.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
      return;
    }

    if (selectedRestaurant == null) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Select a Restaurant'),
            content: Text(
                'Please select a restaurant before saving the diary entry.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
      return;
    }

    Navigator.pop(context);

    final restaurantId = selectedRestaurant!.id;
    final restaurantName = selectedRestaurant!.name;
    Provider.of<DiaryViewModel>(context, listen: false).saveDiaryEntry(
        title, content, DateTime.now(), restaurantId, restaurantName);

    Provider.of<DiaryService>(context, listen: false).updateTitle('');
    Provider.of<DiaryService>(context, listen: false).updateContent('');
    Provider.of<DiaryService>(context, listen: false).saveDiaryToPreferences();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Add a Diary Entry',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Stack(children: [
        Container(
          padding: EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              if (isDataSaved) ...[
                Text(
                  'Your information was saved locally when you tried to save the diary entry but had no internet connection. You can erase it or restore it.',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _controller.text = '';
                              _controller2.text = '';
                              title = '';
                              content = '';
                              isDataSaved = false;
                              Provider.of<DiaryService>(context, listen: false)
                                  .updateTitle('');
                              Provider.of<DiaryService>(context, listen: false)
                                  .updateContent('');
                              Provider.of<DiaryService>(context, listen: false)
                                  .saveDiaryToPreferences();
                            });
                          },
                          child: Text('Erase'),
                          style: ButtonStyle(
                            textStyle: MaterialStateProperty.all<TextStyle>(
                              TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 17.0,
                              ),
                            ),
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (states) {
                                if (states.contains(MaterialState.disabled)) {
                                  return Colors.grey;
                                }
                                return Theme.of(context).primaryColor;
                              },
                            ),
                            foregroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (states) {
                                if (states.contains(MaterialState.disabled)) {
                                  return Colors.white.withOpacity(0.5);
                                }
                                return Colors.white;
                              },
                            ),
                            padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 10.0),
                            ),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                            ),
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          setState(() {
                            isDataSaved = false;
                          });
                        },
                        child: Text('Restore'),
                        style: ButtonStyle(
                          textStyle: MaterialStateProperty.all<TextStyle>(
                            TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 17.0,
                            ),
                          ),
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (states) {
                              if (states.contains(MaterialState.disabled)) {
                                return Colors.grey;
                              }
                              return Theme.of(context).primaryColor;
                            },
                          ),
                          foregroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (states) {
                              if (states.contains(MaterialState.disabled)) {
                                return Colors.white.withOpacity(0.5);
                              }
                              return Colors.white;
                            },
                          ),
                          padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 10.0),
                          ),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
              Spacer(),
              DropdownButton<Restaurant>(
                value: selectedRestaurant,
                hint: Text(
                  'Select a restaurant',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                items: restaurants.map((restaurant) {
                  return DropdownMenuItem<Restaurant>(
                    value: restaurant,
                    child: Text(restaurant.name),
                  );
                }).toList(),
                onChanged: (restaurant) {
                  setState(() {
                    selectedRestaurant = restaurant;
                  });
                },
              ),
              Spacer(),
              TextField(
                controller: _controller,
                maxLength: 50,
                decoration: InputDecoration(
                  hintText: 'Enter title here',
                ),
                onChanged: (value) {
                  setState(() {
                    title = value;
                  });
                },
              ),
              TextField(
                controller: _controller2,
                maxLength: 250,
                decoration: InputDecoration(
                  hintText: 'Enter content here',
                ),
                onChanged: (value2) {
                  setState(() {
                    content = value2;
                  });
                },
              ),
              SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: title.isEmpty
                    ? null
                    : () async {
                        if (!Provider.of<ConnectivityService>(context,
                                listen: false)
                            .isConnected) {
                          Provider.of<DiaryService>(context, listen: false)
                              .updateTitle(title);
                          Provider.of<DiaryService>(context, listen: false)
                              .updateContent(content);
                          Provider.of<DiaryService>(context, listen: false)
                              .saveDiaryToPreferences();
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('No Internet Connection'),
                              content: Text(
                                  'There is no internet connection, so the information of the restaurants cannot be uploaded correctly to the diary entry. The information that you wrote will be saved locally if you want to restore it after the connection is restored.'),
                              actions: [
                                TextButton(
                                  onPressed: () => Navigator.of(context).pop(),
                                  child: Text('OK'),
                                ),
                              ],
                            ),
                          );
                        } else {
                          if (content.isNotEmpty) {
                            submitDiary();
                          }
                        }
                      },
                child: Text('Save'),
                style: ButtonStyle(
                  textStyle: MaterialStateProperty.all<TextStyle>(
                    TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                    ),
                  ),
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (states) {
                      if (states.contains(MaterialState.disabled)) {
                        return Colors.grey;
                      }
                      return Theme.of(context).primaryColor;
                    },
                  ),
                  foregroundColor: MaterialStateProperty.resolveWith<Color>(
                    (states) {
                      if (states.contains(MaterialState.disabled)) {
                        return Colors.white.withOpacity(0.5);
                      }
                      return Colors.white;
                    },
                  ),
                  padding: MaterialStateProperty.all<EdgeInsets>(
                    EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  ),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: !Provider.of<ConnectivityService>(context).isConnected
              ? Container(
                  color: Color(0xFFFE6454),
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Center(
                      child: Text(
                        'Senefood offline',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                )
              : Container(),
        ),
      ]),
    );
  }
}

