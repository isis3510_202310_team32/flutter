//import 'package:flutter/src/widgets/framework.dart';
//import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/restaurant_service.dart';

import '../../models/restaurant.dart';
import '../../view_models/stack_service.dart';

class MapView extends StatefulWidget {
  @override
  State<MapView> createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  bool _switchValue = true;

  Set<Marker> _markers = {};

  void _onMapCreated(GoogleMapController controller) {
    // Add markers to the map
    Set<Marker> newMarkers = {};
    List<Restaurant> restaurants =
        Provider.of<RestaurantService>(context, listen: false).restaurants;
    for (var res in restaurants) {
      newMarkers.add(
        Marker(
          markerId: MarkerId(res.id),
          position: LatLng(res.coordinates.latitude, res.coordinates.longitude),
          infoWindow: InfoWindow(title: res.name),
        ),
      );
    }
    setState(() {
      _markers = newMarkers;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              backgroundColor: Colors.white,
              title: Text(
                'Restaurants',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                          child: TextField(
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(50),
                            ],
                            cursorColor: Colors.black,
                            style: TextStyle(
                                color: Color.fromRGBO(60, 60, 67, 0.6)),
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.search,
                                  color: Color.fromRGBO(60, 60, 67, 0.6)),
                              contentPadding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                              filled: true,
                              fillColor: Color.fromRGBO(118, 118, 128, 0.12),
                              hintText: 'Search',
                              hintStyle: TextStyle(
                                  color: Color.fromRGBO(60, 60, 67, 0.6)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Container(
                              margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                              child: Row(children: [
                                Icon(Icons.filter_list,
                                    color: Theme.of(context).primaryColor),
                                SizedBox(width: 10),
                                Text(
                                  'Filter',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 17),
                                )
                              ]))),
                      Container(
                          margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                          child: Row(children: [
                            Text(
                              'Map view',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17),
                            ),
                            SizedBox(width: 10),
                            CupertinoSwitch(
                              value: _switchValue,
                              onChanged: (bool value) {
                                Provider.of<StackService>(context,
                                        listen: false)
                                    .onItemTapped(0, 0);
                              },
                            ),
                          ]))
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  Expanded(
                    child: Provider.of<RestaurantService>(context)
                            .restaurants
                            .isEmpty
                        ? SizedBox()
                        : GoogleMap(
                            onMapCreated: _onMapCreated,
                            myLocationButtonEnabled: true,
                            myLocationEnabled: true,
                            zoomControlsEnabled: true,
                            initialCameraPosition: CameraPosition(
                              target: LatLng(4.6014581, -74.0661334),
                              zoom: 18,
                            ),
                            markers: _markers,
                          ),
                  ),
                ],
              ),
            )));
  }
}
