import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  static const ROUTE = "splash";
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Center(
          child: Text('SPLASH'),
        ),
      ),
    );
  }
}
