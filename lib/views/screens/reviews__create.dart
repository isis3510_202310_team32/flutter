import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/models/user.dart';
import 'package:senefood/view_models/event_service.dart';
import 'package:senefood/view_models/user_service.dart';

import '../../models/restaurant.dart';
import '../../view_models/connectivity_service.dart';
import '../../view_models/rating_service.dart';
import '../../view_models/restaurant_service.dart';
import '../../view_models/reviews_view_model.dart';

class ReviewsCreate extends StatefulWidget {
  final ReviewsViewModel reviewsViewModel;
  final Restaurant restaurant;

  const ReviewsCreate({
    Key? key,
    required this.reviewsViewModel,
    required this.restaurant,
  }) : super(key: key);

  @override
  _ReviewsCreateState createState() => _ReviewsCreateState();
}

class _ReviewsCreateState extends State<ReviewsCreate> {
  int stars = 0;
  String reviewText = '';
  bool isConnected = true;

  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    initValues();
  }

  void initValues() async {
    final ratingService = Provider.of<RatingService>(context, listen: false);
    ratingService.loadRatingFromPreferences();
    final savedStars = ratingService.getStars();
    final savedText = ratingService.getText();

    setState(() {
      stars = savedStars;
      reviewText = savedText;
      _controller.text = savedText;
    });
  }

  void submitReview() async {
    // check for connectivity
    isConnected =
        Provider.of<ConnectivityService>(context, listen: false).isConnected;
    if (!isConnected) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No Connectivity'),
            content:
                Text('Please check your internet connection and try again.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
      return;
    }

    final EventService eventService = EventService();
    print("Testing EVEENT");
    await eventService.sendEventRestaurantRatingReview(
        widget.restaurant.name, stars);
    // send to the view model
    final AuthUser user =
        Provider.of<AuthUserService>(context, listen: false).getCurrentUser();
    await Provider.of<RestaurantService>(context, listen: false).addReview(
        widget.restaurant.id, user.getDisplayName, stars, reviewText);
    await Provider.of<RestaurantService>(context, listen: false)
        .getAllRestaurantsProvider();
    await Provider.of<RestaurantService>(context, listen: false)
        .recalculateGlobalScore(widget.restaurant.id, stars, reviewText);
    Navigator.pop(context);

    Provider.of<RatingService>(context, listen: false).updateText('');
    Provider.of<RatingService>(context, listen: false).updatestars(0);
    Provider.of<RatingService>(context, listen: false).saveRatingToPreferences;

    /* Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ReviewsView(
                    reviewsViewModel: _reviewsViewModel,
                     restaurant: widget.restaurant
                  )));
                  */
  }

  Widget buildStarButton(int starCount) {
    return IconButton(
      icon: Icon(
        stars >= starCount ? Icons.star : Icons.star_border,
        color: stars >= starCount ? Color(0xFFfccc04) : Colors.grey,
      ),
      onPressed: () {
        setState(() {
          stars = starCount;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Add a Review',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildStarButton(1),
                buildStarButton(2),
                buildStarButton(3),
                buildStarButton(4),
                buildStarButton(5),
              ],
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: _controller,
              maxLength: 150,
              decoration: InputDecoration(
                hintText: 'Enter your review here',
              ),
              onChanged: (value) {
                setState(() {
                  reviewText = value;
                });
              },
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: reviewText.isEmpty
                  ? null
                  : () async {
                      if (!Provider.of<ConnectivityService>(context,
                              listen: false)
                          .isConnected) {
                        Provider.of<RatingService>(context, listen: false)
                            .updateText(reviewText);
                        Provider.of<RatingService>(context, listen: false)
                            .updatestars(stars);
                        Provider.of<RatingService>(context, listen: false)
                            .saveRatingToPreferences;
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('No Internet Connection'),
                            content: Text(
                                'The review cannot be sent without internet connection. The content of  your review will be saved.'),
                            actions: [
                              TextButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text('OK'),
                              ),
                            ],
                          ),
                        );
                      } else {
                        if (reviewText.isNotEmpty) {
                          submitReview();
                        }
                      }
                    },
              style: ButtonStyle(
                textStyle: MaterialStateProperty.all<TextStyle>(
                  TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 17.0,
                  ),
                ),
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (states) {
                    if (states.contains(MaterialState.disabled)) {
                      return Colors.grey;
                    }
                    return Theme.of(context).primaryColor;
                  },
                ),
                foregroundColor: MaterialStateProperty.resolveWith<Color>(
                  (states) {
                    if (states.contains(MaterialState.disabled)) {
                      return Colors.white.withOpacity(0.5);
                    }
                    return Colors.white;
                  },
                ),
                padding: MaterialStateProperty.all<EdgeInsets>(
                  EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              ),
              child: Text('Submit'),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
