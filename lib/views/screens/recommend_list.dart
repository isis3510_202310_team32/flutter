import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/views/screens/restaurant_list.dart';

import '../../view_models/connectivity_service.dart';
import '../../view_models/stack_service.dart';
import '../../view_models/user_service.dart';

class RecommendList extends StatefulWidget {
  @override
  State<RecommendList> createState() => _RecommendListState();
}

class _RecommendListState extends State<RecommendList> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      String userId = Provider.of<AuthUserService>(context, listen: false)
              .getCurrentUser()
              .username ??
          'dummyUser';
      Provider.of<RestaurantService>(context, listen: false)
          .getRecommendedRestaurantsProvider(userId);
    });
  }

  void handleLostConnectivity() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<StackService>(context, listen: false).onItemTapped(2, 2);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!Provider.of<ConnectivityService>(context).isConnected) {
      handleLostConnectivity();
    }
    return WillPopScope(
      onWillPop: () async {
        Provider.of<StackService>(context, listen: false).onItemTapped(2, 2);
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
            leading: BackButton(
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  int likes =
                      Provider.of<RestaurantService>(context, listen: false)
                          .likesList
                          .length;
                  int dislikes =
                      Provider.of<RestaurantService>(context, listen: false)
                          .dislikesList
                          .length;
                  int recommendations =
                      Provider.of<RestaurantService>(context, listen: false)
                          .recommendations
                          .length;
                  Provider.of<RestaurantService>(context, listen: false)
                      .sendEventRecommendations(
                          recommendations, likes, dislikes);
                  Provider.of<StackService>(context, listen: false)
                      .onItemTapped(2, 2);
                }),
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Recommendations',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Padding(
            padding:
                const EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Recommended for you',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                  Provider.of<RestaurantService>(context).loadingRecommendations
                      ? Center(
                          child: Padding(
                          padding: EdgeInsets.all(50.0),
                          child: CircularProgressIndicator(),
                        ))
                      : RestaurantList(
                          recommend: true,
                          recommendations:
                              Provider.of<RestaurantService>(context)
                                  .recommendations,
                          changeLike: Provider.of<RestaurantService>(context)
                              .changeLike,
                          likesList:
                              Provider.of<RestaurantService>(context).likesList,
                          dislikesList: Provider.of<RestaurantService>(context)
                              .dislikesList,
                          changeList: Provider.of<RestaurantService>(context)
                              .changeList,
                          dislikes: false),
                  Padding(
                    padding: EdgeInsets.fromLTRB(8, 20, 0, 0),
                    child: Text(
                      'Dislikes',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                  Provider.of<RestaurantService>(context).loadingRecommendations
                      ? Center(
                          child: Padding(
                          padding: EdgeInsets.all(50.0),
                          child: CircularProgressIndicator(),
                        ))
                      : RestaurantList(
                          recommend: true,
                          recommendations:
                              Provider.of<RestaurantService>(context)
                                  .recommendations,
                          changeLike: Provider.of<RestaurantService>(context)
                              .changeLike,
                          likesList:
                              Provider.of<RestaurantService>(context).likesList,
                          dislikesList: Provider.of<RestaurantService>(context)
                              .dislikesList,
                          changeList: Provider.of<RestaurantService>(context)
                              .changeList,
                          dislikes: true),
                ],
              ),
            ),
          )),
    );
  }
}
