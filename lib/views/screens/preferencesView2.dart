import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/models/user.dart';
import 'package:senefood/view_models/stack_service.dart';
import 'package:senefood/view_models/user_service.dart';

class PreferencesView2 extends StatefulWidget {
  const PreferencesView2({super.key});

  @override
  State<PreferencesView2> createState() => _PreferencesView2State();
}

class _PreferencesView2State extends State<PreferencesView2> {

  List<String>? foodPreferences = [];

  @override
  Widget build(BuildContext context) {
    AuthUser user = Provider.of<AuthUserService>(context, listen: false).getCurrentUser();
    
    return Material(
      child: Scaffold(
        body: Padding(
          padding:
              const EdgeInsets.only(top: 200, right: 8, left: 8, bottom: 8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Welcome! \n ${user.getDisplayName!}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 28,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Center(
                    child: Text(
                      'Choose your food preferences',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                Column(
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      ColorChangingButton(text: 'Italian'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Asian'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Mexican'),
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      ColorChangingButton(text: 'Fast Food'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Wok'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Colombian'),
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      ColorChangingButton(text: 'Vegetarian'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Arab'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Elegant'),
                    ]),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      ColorChangingButton(text: 'Snacks'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Dessert'),
                      SizedBox(width: 5),
                      ColorChangingButton(text: 'Burgers'),
                    ]),
                    SizedBox(
                      height: 150,
                    ),
                    SizedBox(
                      width: 250,
                      height: 60,
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            foodPreferences = user.getPreferences;
                          });
                          Provider.of<StackService>(context, listen: false)
                              .onItemTapped(0, 0);
                          Provider.of<AuthUserService>(context, listen: false).assignSelectedPreferences(foodPreferences);
                          Provider.of<AuthUserService>(context, listen: false).changePreferences(foodPreferences);
                          print("Preferencias agregadas: ${user.getPreferences}");
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.0),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                        ),
                        child: Text(
                          'Ready',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ColorChangingButton extends StatefulWidget {
  const ColorChangingButton({Key? key, required this.text}) : super(key: key);

  final String text;

  @override
  _ColorChangingButtonState createState() => _ColorChangingButtonState();
}

class _ColorChangingButtonState extends State<ColorChangingButton> {
  bool _isPressed = false;


  @override
  Widget build(BuildContext context) { //MICRO-OPTIMIZACION
    return ElevatedButton(
      onPressed: () {
        setState(() {
          _isPressed = !_isPressed;
        });
        if(_isPressed){
          List<String> userPreferences =[];
          
          userPreferences.add(widget.text);
          Provider.of<AuthUserService>(context, listen: false).addFoodPreference(widget.text);
          print("Preference ${widget.text} added successfuly");
        }
        
      },
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (_isPressed) {
              return Theme.of(context).primaryColor;
            }
            return Colors.white;
          },
        ),
      ),
      child: Text(
        widget.text,
        style: TextStyle(
          color: _isPressed ? Colors.white : Colors.black,
        ),
      ),
    );
  }
}