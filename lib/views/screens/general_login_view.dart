import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/stack_service.dart';
import 'package:senefood/view_models/user_service.dart';
import 'package:senefood/views/widgets/unavailable_dialog.dart';

import '../../models/user.dart';

class LoginView extends StatefulWidget {
  //final AuthUserService userService;
  //final String userId;
  // const LoginView(
  //     {required this.userId});

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  Future<void> handleOnPressed() async {
    bool isConnected =
        Provider.of<ConnectivityService>(context, listen: false).isConnected;
    if (isConnected) {
      AuthUser user = await Provider.of<AuthUserService>(context, listen: false)
          .authenticate();
      if (await Provider.of<AuthUserService>(context, listen: false)
          .isUserLogged()) {
        Provider.of<StackService>(context, listen: false).onItemTapped(-1, 7);
      }
    } else {
      showUnavailableDialog(context);
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (await Provider.of<AuthUserService>(context, listen: false)
          .isUserLogged()) {
        Provider.of<StackService>(context, listen: false).onItemTapped(0, 0);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFED6F5B),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 230.0),
              child: Center(
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: Colors.white,
                        width: 10,
                      ),
                    ),
                    child: Image.asset('assets/SeneFood_Logo.png')),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              margin: EdgeInsets.all(30),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: ElevatedButton(
                onPressed: () {
                  handleOnPressed();
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
                child: Text(
                  'Login with Uniandes',
                  style: TextStyle(
                    color: Provider.of<ConnectivityService>(context).isConnected
                        ? Color(0xFFED6F5B)
                        : Color(0xFFe3e3e3),
                    fontWeight: FontWeight.w600,
                    fontSize: 17.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
