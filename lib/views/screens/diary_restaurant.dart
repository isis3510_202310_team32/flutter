import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/diary_view_model.dart';
import 'package:senefood/views/screens/diary__create.dart';
import 'package:senefood/views/screens/reviews_row.dart';
import 'package:senefood/views/screens/diary_detail_view.dart';

import '../../models/restaurant.dart';
import '../../view_models/connectivity_service.dart';
import '../../view_models/restaurant_service.dart';
import 'package:intl/intl.dart';

class DiaryRestaurantView extends StatefulWidget {
  @override
  _DiaryRestaurantViewState createState() => _DiaryRestaurantViewState();
}

class _DiaryRestaurantViewState extends State<DiaryRestaurantView> {
  late DiaryViewModel _DiaryViewModel;

  @override
  void initState() {
    super.initState();
    _DiaryViewModel = Provider.of<DiaryViewModel>(context, listen: false);
    _DiaryViewModel.initialize().then((_) {
      _DiaryViewModel.fetchDiaryEntries();
    });
  }

  @override
  Widget build(BuildContext context) {
    final _DiaryViewModel = Provider.of<DiaryViewModel>(context);
    Set<String> uniqueRestaurants = Set<String>();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Diary Restaurant Summary',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          
        ],
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Stack(children:[Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
                Spacer(),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Restaurants that you have written about \n in the last two weeks',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      ListView.builder(
                        itemCount: _DiaryViewModel.diaryEntries.length,
                        itemBuilder: (context, index) {
                          final entry = _DiaryViewModel.diaryEntries[index];
                          if (!uniqueRestaurants
                                  .contains(entry.restaurantName) &&
                              isTwoWeeks(entry.date)) {
                            uniqueRestaurants.add(entry.restaurantName);
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 8.0),
                              child: Center(
                                child: Text(
                                  entry.restaurantName,
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            );
                          } else {
                            return Container();
                          }
                        },
                        shrinkWrap: true,
                        physics: AlwaysScrollableScrollPhysics(),
                      ),
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
        ),
         Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: !Provider.of<ConnectivityService>(context).isConnected
                ? Container(
                    color: Color(0xFFFE6454),
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: Text(
                          'Senefood offline',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ),
      ]
      ),
      ); 
    
  }

  bool isTwoWeeks(DateTime date) {
    final now = DateTime.now();
    final twoWeeksAgo = now.subtract(Duration(days: 14));
    return date.isAfter(twoWeeksAgo) && date.isBefore(now);
  }
}
