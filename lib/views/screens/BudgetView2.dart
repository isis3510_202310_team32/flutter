import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/database_service.dart';
import 'package:senefood/view_models/stack_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'budget_pending_view.dart';

class BudgetView2 extends StatefulWidget {
  const BudgetView2({super.key});

  @override
  State<BudgetView2> createState() => _BudgetView2State();
}

class _BudgetView2State extends State<BudgetView2> {
  List<dynamic> dishes = [];
  List<dynamic> daysBudget = [];
  num totalPrice = 0;
  List<String> daysList = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday'
  ];
  double budgetPercentage = 0;
  int budget = 0;

  void getBudgetDays() async {
    List<dynamic> newBudget =
        await Provider.of<DatabaseService>(context, listen: false)
            .queryAllRows();
    setState(() {
      dishes = newBudget;
    });
    List<dynamic> newDaysBudget = [];
    for (var day in daysList) {
      Map<String, dynamic> dayBudget = {'price': 0, 'day': day};
      for (var dish in dishes) {
        if (dish['day'] == day) {
          dayBudget['price'] += dish['price'];
          totalPrice += dish['price'];
        }
      }
      if (dayBudget['price'] > 0) {
        newDaysBudget.add(dayBudget);
      }
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      daysBudget = newDaysBudget;
      budget = prefs.getInt('budget') ?? 0;
      budgetPercentage = prefs.getDouble('budgetPercentage') ?? 0;
    });
  }

  @override
  void initState() {
    super.initState();
    getBudgetDays();
  }

  @override
  Widget build(BuildContext context) {
    if (dishes.isNotEmpty) {
      return Material(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Budget',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Daily estimated costs',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                RectangleWidget(
                  daysBudget: daysBudget,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'How much are you saving?',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
                Container(
                  width: 320,
                  padding: EdgeInsets.only(top: 5),
                  child: Text(
                    'Based on your weekly \$$budget pesos budget you will be saving \$${budget - totalPrice}! Great!',
                    style: TextStyle(
                      fontSize: 19.0,
                      height: 1.3,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: ElevatedButton(
                      onPressed: () {
                        Provider.of<StackService>(context, listen: false)
                            .onItemTapped(1, 10);
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).primaryColor),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      child: Text(
                        'View Dishes',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 17.0,
                        ),
                      ),
                    ),
                  ),
                ),
                Center(
                    child: Container(
                  margin: EdgeInsets.all(1),
                  child: ElevatedButton(
                    onPressed: () {
                      Provider.of<StackService>(context, listen: false)
                          .onItemTapped(1, 9);
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).splashColor),
                      padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.only(
                            left: 27, right: 27, top: 17, bottom: 17),
                      ),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14),
                        ),
                      ),
                    ),
                    child: Text(
                      'Configure Budget',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 17.0,
                      ),
                    ),
                  ),
                )),
              ],
            ),
          ),
        ),
      );
    } else {
      return BudgetPendingView();
    }
  }
}

class RectangleWidget extends StatefulWidget {
  const RectangleWidget({Key? key, required this.daysBudget}) : super(key: key);

  final List<dynamic> daysBudget;

  @override
  _RectangleWidgetState createState() => _RectangleWidgetState();
}

class _RectangleWidgetState extends State<RectangleWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: widget.daysBudget
            .map((day) => DailyCost(day['day'], day['price'].toDouble()))
            .toList());
  }

  Widget DailyCost(String text, double size) {
    return Row(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 20, top: 17, bottom: 17),
          width: 70,
          child: Text(text.substring(0, 3),
              style: TextStyle(
                fontSize: 17,
              )),
        ),
        Container(
          width: size / 100,
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Center(
            child: Text(
              NumberFormat.currency(symbol: '\$', decimalDigits: 0)
                  .format(size),
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
