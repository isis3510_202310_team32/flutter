import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/views/widgets/budget_card.dart';

import '../../view_models/database_service.dart';
import '../../view_models/stack_service.dart';

class BudgetList extends StatefulWidget {
  const BudgetList({Key? key}) : super(key: key);

  @override
  State<BudgetList> createState() => _BudgetListState();
}

class _BudgetListState extends State<BudgetList> {
  List<dynamic> dishes = [];
  List<String> daysList = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday'
  ];

  void getBudgetDays() async {
    List<dynamic> newBudget =
        await Provider.of<DatabaseService>(context, listen: false)
            .queryAllRows();
    setState(() {
      dishes = newBudget;
    });
  }

  @override
  void initState() {
    super.initState();
    getBudgetDays();
  }

  List<Widget> getListWidgets() {
    List<Widget> budgetList = [];
    for (var day in daysList) {
      List<Widget> dayWidgets = [];

      for (var dish in dishes) {
        if (dish['day'] == day) {
          dayWidgets.add(BudgetCard(dish: dish));
          dayWidgets.add(SizedBox(
            height: 10,
          ));
        }
      }

      if (dayWidgets.isNotEmpty) {
        budgetList.add(Text(
          day,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ));
        budgetList.addAll(dayWidgets);
      }
    }
    return budgetList;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Provider.of<StackService>(context, listen: false).onItemTapped(1, 1);
        return false;
      },
      child: Material(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Dishes',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: getListWidgets(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
