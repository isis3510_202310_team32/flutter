import 'package:flutter/material.dart';
import 'package:senefood/view_models/reviews_view_model.dart'; 

class ReviewsRow extends StatelessWidget {
  final dynamic review;
  final ReviewsViewModel reviewsViewModel;

  const ReviewsRow({
    Key? key,
    required this.review,
    required this.reviewsViewModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: 
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                review['user'],
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
              ),
              SizedBox(height: 4),
              SizedBox(
                height: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(
                    review['stars'],
                    (index) => Icon(
                      Icons.star,
                       color: Color(0xFFfccc04),
                      size: 20,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16),
               SizedBox(
                width: 300, 
                child: Text(
                             review['text'],
                             style: TextStyle(fontSize: 14, color: Colors.black),
                             overflow: TextOverflow.clip,
                           ),
               ),
            ],
          ),
          
    
    );
  }
}