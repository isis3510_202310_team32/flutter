import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/diary_view_model.dart';
import 'package:senefood/views/screens/diary__create.dart';
import 'package:senefood/views/screens/diary_restaurant.dart';

import 'package:senefood/views/screens/reviews_row.dart';
import 'package:senefood/views/screens/diary_detail_view.dart';

import 'package:intl/intl.dart';

class DiaryView extends StatefulWidget {
  @override
  _DiaryViewState createState() => _DiaryViewState();
}

class _DiaryViewState extends State<DiaryView> {
  late DiaryViewModel _diaryViewModel;

  @override
  void initState() {
    super.initState();
    _diaryViewModel = Provider.of<DiaryViewModel>(context, listen: false);
    _diaryViewModel.initialize().then((_) {
      _diaryViewModel.fetchDiaryEntries();
    });
  }

  @override
  Widget build(BuildContext context) {
    final diaryViewModel = Provider.of<DiaryViewModel>(context);
    Set<String> uniqueRestaurants = <String>{};

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Diary',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: [
          IconButton(
            icon: Icon(Icons.add, color: Theme.of(context).primaryColor),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DiaryCreate(
                            diaryViewModel: diaryViewModel,
                          )));
            },
          ),
          IconButton(
            icon: Icon(Icons.food_bank, color: Theme.of(context).primaryColor),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DiaryRestaurantView()));
            },
          )
        ],
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    'Your diary entries',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Stack(
                  children: [
                    ListView.builder(
                      itemCount: _diaryViewModel.diaryEntries.length,
                      itemBuilder: (context, index) {
                        final entry = _diaryViewModel.diaryEntries[index];
                        return ListTile(
                          title: Text(entry.title),
                          subtitle:
                              Text(DateFormat('yyyy-MM-dd').format(entry.date)),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    DiaryDetailView(entry: entry),
                              ),
                            );
                          },
                        );
                      },
                      shrinkWrap: true,
                      physics: AlwaysScrollableScrollPhysics(),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: !Provider.of<ConnectivityService>(context).isConnected
                ? Container(
                    color: Color(0xFFFE6454),
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: Text(
                          'Senefood offline',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
