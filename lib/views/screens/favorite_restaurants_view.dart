import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/models/restaurant.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/view_models/user_service.dart';
import 'package:senefood/views/widgets/restaurant_card.dart';

class FavoriteRestaurantsView extends StatefulWidget {
  @override
  _FavoriteRestaurantsViewState createState() => _FavoriteRestaurantsViewState();
}

class _FavoriteRestaurantsViewState extends State<FavoriteRestaurantsView> {
  List<String> favoritesIds = [];
  List<Restaurant> favRests = [];
  List<RestaurantCard> favRestsCards =[];
  Map<String, int> distances ={};

  List<Key> _listItemKeys = [
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),
    UniqueKey(),

  ];

  @override
  void initState(){
    super.initState();
    initializeFavRestaurants();
  }
  
  void initializeFavRestaurants() async {
    List<String> favRestaurantsIds = await Provider.of<AuthUserService>(context, listen: false).getFavoriteRestaurantsIdsFromDB();
    setState(() {
      favoritesIds = favRestaurantsIds;
    });
  }

  @override
  Widget build(BuildContext context) {
    distances = Provider.of<RestaurantService>(context, listen: false).distances;

    for(int i=0; i<favoritesIds.length; i++){
      favRests.add(Provider.of<RestaurantService>(context, listen:false).getRestaurant(favoritesIds[i]));
      favRestsCards.add(RestaurantCard(
        restaurant: favRests[i], 
        distance: distances[favRests[i].id]));
    }

    if (!Provider.of<ConnectivityService>(context, listen: false).isConnected){
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('No Internet Connection'),
          content: Text(
            'Your favorite restaurants may not appear or may not show correctly.'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('OK')),
            ],
        ),
      );
    }
    
    if(favRests.isEmpty){
       return Scaffold(
         appBar: AppBar(
           centerTitle: true,
           backgroundColor: Colors.white,
           title: Text(
             'Your Favorite Restaurants',
             style: TextStyle(
               color: Colors.black,
             ),
           ),
         ),
         body: Center(
             child: Padding(
               padding: EdgeInsets.all(20.0),
               child: Column(
                 children: [
                   Icon(Icons.search, size: 50),
                   SizedBox(
                     height: 10,
                   ),
                   SizedBox(
                     width: 250,
                     child: Text(
                         'Ups, you do not have any favorite restaurants',
                         textAlign: TextAlign.center),
                   )
                 ],
               ),
             ),
           ),
       );

    }
    
    return Scaffold(
         appBar: AppBar(
           centerTitle: true,
           backgroundColor: Colors.white,
           title: Text(
             'Your Favorite Restaurants',
             style: TextStyle(
               color: Colors.black,
             ),
           ),
         ),
         body: ReorderableListView(
          children: favRestsCards
              .asMap().entries.map((restaurant) {
                final index = restaurant.key;
                final item = restaurant.value;

                return Dismissible(
              key: _listItemKeys[index],
              background: Container(color: Colors.red),
              onDismissed: (direction) {
                setState(() {
                  Provider.of<AuthUserService>(context, listen: false).removeFavoriteRestaurantFromDB(item.restaurant.id);
                  favRestsCards.removeAt(index);
                  _listItemKeys.removeAt(index);
                });
              },
              child: RestaurantCard(distance: item.distance, restaurant: item.restaurant)
            );
              }) 
              .toList(),
            onReorder: (oldIndex, newIndex) {
            setState(() {
              if (newIndex > oldIndex) {
                newIndex --;
              }
              final item = favRestsCards.removeAt(oldIndex);
              final key = _listItemKeys.removeAt(oldIndex);
              favRestsCards.insert(newIndex, item);
              _listItemKeys.insert(newIndex, key);
            });
          },
        ),
      );
  }
}
