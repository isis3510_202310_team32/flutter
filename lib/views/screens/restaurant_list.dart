import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/models/restaurant.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/views/widgets/restaurant_card.dart';
import 'package:senefood/views/widgets/restaurant_card_like.dart';

class RestaurantList extends StatefulWidget {
  List<String>? visited;
  bool? recommend;
  bool? location;
  List<dynamic>? recommendations;
  bool? dislikes;
  Function(String, int)? changeLike;
  Function(dynamic, dynamic)? changeList;
  List<String>? likesList;
  List<String>? dislikesList;
  bool? isFiltered;
  RestaurantList(
      {this.visited,
      this.recommend,
      this.recommendations,
      this.changeLike,
      this.changeList,
      this.likesList,
      this.dislikesList,
      this.dislikes,
      this.location,
      this.isFiltered});

  @override
  _RestaurantListState createState() => _RestaurantListState();
}

class _RestaurantListState extends State<RestaurantList> {
  late RestaurantService restaurantService;

  @override
  void initState() {
    super.initState();
    restaurantService = Provider.of<RestaurantService>(context, listen: false);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      restaurantService.getAllRestaurantsProvider();
    });
    if (widget.location ?? false) {
      restaurantService.initPositionStream();
    }
  }

  @override
  void dispose() {
    if (widget.location ?? false) {
      restaurantService.disposePositionStream();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    RestaurantService provider = Provider.of<RestaurantService>(context);
    List<Restaurant> restaurants = provider.restaurants;
    List<Restaurant> restaurantRecommendationsLikes =
        provider.restaurantRecommendationsLikes;
    List<Restaurant> restaurantRecommendationsDislikes =
        provider.restaurantRecommendationsDislikes;
    Function filterByVisited = provider.filterByVisited;
    Function filterAndSortRestaurants = provider.filterAndSortRestaurants;
    Function filterAndSortByLocation = provider.filterAndSortByLocation;
    Map<String, int> distances =
        Provider.of<RestaurantService>(context).distances;
    bool loadingRestaurants = provider.loadingRestaurants;
    bool recommend = widget.recommend ?? false;
    if (widget.visited != null) {
      dynamic visited = widget.visited;
      if (visited.isNotEmpty) {
        restaurants = filterByVisited(restaurants, visited);
      }
    } else if (recommend) {
      dynamic dislikes = widget.dislikes;
      if (dislikes) {
        restaurants = restaurantRecommendationsDislikes;
      } else {
        restaurants = restaurantRecommendationsLikes;
      }
    } else if (widget.location ?? false) {
      restaurants = filterAndSortByLocation(restaurants);
    } else if (widget.isFiltered ?? false) {
      restaurants = filterAndSortRestaurants(restaurants);
    }
    dynamic changeLike = widget.changeLike;
    if (loadingRestaurants) {
      return Center(
          child: Padding(
        padding: EdgeInsets.all(50.0),
        child: CircularProgressIndicator(),
      ));
    }
    if (restaurants.isEmpty) {
      if (widget.location ?? false) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.directions_run, size: 100, color: Color(0xFFFE6454)),
              SizedBox(height: 20),
              Text(
                  'Keep walking! We are looking for restaurants around for you',
                  textAlign: TextAlign.center)
            ],
          ),
        );
      } else {
        return Center(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Icon(Icons.search, size: 50),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: 250,
                  child: Text(
                      'Ups, no se encontraron resultados para tu busqueda',
                      textAlign: TextAlign.center),
                )
              ],
            ),
          ),
        );
      }
    }

    return SingleChildScrollView(
      child: Column(
        children: restaurants
            .map((restaurant) => recommend
                ? RestaurantCardLike(
                    restaurant: restaurant,
                    like: Provider.of<RestaurantService>(context)
                        .getLikeNumber(restaurant),
                    changeLike: changeLike)
                : RestaurantCard(
                    restaurant: restaurant,
                    distance: distances[restaurant.id] ?? 0))
            .toList(),
      ),
    );
  }
}
