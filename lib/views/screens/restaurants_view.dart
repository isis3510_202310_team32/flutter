import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/permissions_service.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/view_models/stack_service.dart';
import 'package:senefood/views/screens/restaurant_list.dart';
import 'package:senefood/views/widgets/restaurants_filters.dart';

import '../../models/sorting_option.dart';
import '../../view_models/user_service.dart';

class RestaurantsView extends StatefulWidget {
  final String? userId;
  const RestaurantsView({super.key, this.userId});

  @override
  State<RestaurantsView> createState() => _RestaurantsViewState();
}

class _RestaurantsViewState extends State<RestaurantsView> {
  bool _switchValue = false;

  @override
  void initState() {
    super.initState();
    String userId = Provider.of<AuthUserService>(context, listen: false)
            .getCurrentUser()
            .username ??
        'dummyUser';
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<RestaurantService>(context, listen: false)
          .getRecentlyVisitedRestaurantsProvider(userId);
      Provider.of<RestaurantService>(context, listen: false)
          .loadFiltersFromPreferences();
    });
  }

  void _showBottomSheet() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        List<SortingOption> options = [
          SortingOption(name: 'Price', value: 'price'),
          SortingOption(name: 'Reviews', value: 'recommendationScore'),
          SortingOption(name: 'Cleanness Score', value: 'cleannessScore'),
          SortingOption(name: 'Preparation Time', value: 'time')
        ];
        if (Provider.of<PermissionsService>(context).locationPermission) {
          options.add(SortingOption(name: 'Distance', value: 'distance'));
        }
        return Wrap(children: [
          RestaurantFilters(
              sortingOptions: options,
              filters: Provider.of<RestaurantService>(context).filters)
        ]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text(
            'Restaurants',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: Provider.of<RestaurantService>(context).loadingVisited
            ? Center(
                child: Padding(
                padding: EdgeInsets.all(50.0),
                child: CircularProgressIndicator(),
              ))
            : Padding(
                padding:
                    const EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 8),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                              child: TextField(
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(50),
                                ],
                                onChanged: (value) {
                                  Provider.of<RestaurantService>(context,
                                          listen: false)
                                      .setAttributeFilters("name", value);
                                },
                                cursorColor: Colors.black,
                                style: TextStyle(
                                    color: Color.fromRGBO(60, 60, 67, 0.6)),
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.search,
                                      color: Color.fromRGBO(60, 60, 67, 0.6)),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(8, 5, 8, 5),
                                  filled: true,
                                  fillColor:
                                      Color.fromRGBO(118, 118, 128, 0.12),
                                  hintText: 'Search',
                                  hintStyle: TextStyle(
                                      color: Color.fromRGBO(60, 60, 67, 0.6)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.circular(15)),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Container(
                            margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                            child: IconButton(
                                onPressed: _showBottomSheet,
                                icon: Row(children: [
                                  Icon(
                                    Icons.filter_list,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    'Filter',
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 17),
                                  )
                                ])),
                          )),
                          Container(
                              margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                              child: Row(children: [
                                Text(
                                  'Map view',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17),
                                ),
                                SizedBox(width: 10),
                                CupertinoSwitch(
                                  value: _switchValue,
                                  onChanged: (bool value) {
                                    Provider.of<StackService>(context,
                                            listen: false)
                                        .onItemTapped(0, 6);
                                    if (!Provider.of<ConnectivityService>(
                                            context,
                                            listen: false)
                                        .isConnected) {
                                      showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          title: Text('No Internet Connection'),
                                          content: Text(
                                              'The map and its functionalities may not work properly.'),
                                          actions: [
                                            TextButton(
                                                onPressed: () =>
                                                    Navigator.of(context).pop(),
                                                child: Text('OK')),
                                          ],
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ]))
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: Provider.of<RestaurantService>(context)
                                .restaurantsVisited
                                .isNotEmpty
                            ? [
                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    'My recent visits (last two weeks)',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17,
                                    ),
                                  ),
                                ),
                                Provider.of<RestaurantService>(context)
                                        .loadingVisited
                                    ? Center(
                                        child: Padding(
                                        padding: EdgeInsets.all(50.0),
                                        child: CircularProgressIndicator(),
                                      ))
                                    : RestaurantList(
                                        visited: Provider.of<RestaurantService>(
                                                context)
                                            .restaurantsVisited,
                                      ),
                              ]
                            : [Container()],
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'All restaurants',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                        ),
                      ),
                      RestaurantList(
                        isFiltered: true,
                      ),
                    ],
                  ),
                ),
              ));
  }
}
