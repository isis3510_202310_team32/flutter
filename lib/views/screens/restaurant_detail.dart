import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/user_service.dart';
import 'package:senefood/views/widgets/dish_card.dart';
import 'package:senefood/views/widgets/restaurant_card_reviews.dart';

import '../../view_models/connectivity_service.dart';

class RestaurantDetail extends StatefulWidget {
  final dynamic restaurant;
  const RestaurantDetail({Key? key, required this.restaurant})
      : super(key: key);

  @override
  State<RestaurantDetail> createState() => _RestaurantDetailState();
}

class _RestaurantDetailState extends State<RestaurantDetail> {
  bool _isPressed = false;

  @override
  void initState() {
    super.initState();
    //initSavedFavoriteRestaurant();
  }

  //  void initSavedFavoriteRestaurant(){
  //    final favoriteRestaurant = Provider.of<AuthUserService>(context, listen: false)
  //        .isFavoriteRestaurant(widget.restaurant.id);
  //    setState(() {
  //      if(favoriteRestaurant){
  //        _isPressed = true;
  //      }
  //    });
  //  }

  List<Widget> getDishCards() {
    List<dynamic> dishes = widget.restaurant.dishes;
    return List<Widget>.from(dishes.map((dish) => DishCard(dish: dish)));
  }

  Widget buildStarButton(int starCount) {
    
    return IconButton(
      onPressed: () {
        setState(() {
          _isPressed = !_isPressed;
        });
        if (_isPressed){
          Provider.of<AuthUserService>(context, listen: false).addNewFavoriteRestaurantToDB(widget.restaurant.id);  //Aca podria probar cambiar esta linea de codigo con la de abajo en orden a ver si afecta algo!
          Provider.of<AuthUserService>(context, listen: false).addFavoriteRestaurant(widget.restaurant.id);

           print("Adding favorite restaurant ${widget.restaurant.id}...");
        }
        else if (!_isPressed){
          Provider.of<AuthUserService>(context, listen: false).removeFavoriteRestaurantFromDB(widget.restaurant.id);
          Provider.of<AuthUserService>(context, listen: false).removeFavoriteRestaurant(widget.restaurant.id);
          //Remover de la lista de favoritos con el provider usuario
        }
        
      },
      icon: Icon(
        _isPressed ? Icons.star : Icons.star_border,
        color: Color(0xFFfccc04),
      ),      
    );
  }

  @override
  Widget build(BuildContext context) {
    print ("Lista de restaurantes favoritos: ${Provider.of<AuthUserService>(context, listen: true).currentUser.getFavoriteRestaurants}");
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          widget.restaurant.name,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        actions: [buildStarButton(1)],
      ),
      body: Stack(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 56),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RestaurantCardReviews(restaurant: widget.restaurant),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    'All dishes',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Column(
                    children: getDishCards(),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: !Provider.of<ConnectivityService>(context).isConnected
                ? Container(
                    color: Color(0xFFFE6454),
                    height: 30,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Center(
                        child: Text(
                          'Senefood offline',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
