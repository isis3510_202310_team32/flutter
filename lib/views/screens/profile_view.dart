import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/user_service.dart';

import '../../view_models/stack_service.dart';
import '../../views/screens/diary_view.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({super.key});

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    List<String>? preferences =
        Provider.of<AuthUserService>(context, listen: false)
            .currentUser
            .getPreferences;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Profile',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                  fontSize: 17.0)),
          actions: [
            IconButton(
              icon: Icon(Icons.book, color: Theme.of(context).primaryColor),
              onPressed: () {
                Navigator.push(
                    context,
                     MaterialPageRoute(
                      builder: (context) => DiaryView(),
                    ));
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.all(18.0),
          child: Column(children: [
            SizedBox(
              width: 120,
              height: 120,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child:
                      const Image(image: AssetImage("assets/DefaultPP.jpg"))),
            ),
            const SizedBox(height: 10),
            Text(
                Provider.of<AuthUserService>(context).currentUser.displayName ??
                    '',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                    fontSize: 23.0)),
            Row(
              children: [
                Container(
                    margin: EdgeInsets.all(8),
                    child: Text("My preferences",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                            fontSize: 18.0))),
                Container(
                    margin: EdgeInsets.all(5),
                    child: ElevatedButton(
                        onPressed: () {},
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).splashColor),
                          padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.only(
                                left: 17, right: 17, top: 10, bottom: 10),
                          ),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14),
                            ),
                          ),
                        ),
                        child: Text("Edit",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0,
                            )))),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Wrap(
                    alignment: WrapAlignment.center,
                    spacing: 4.0,
                    children: preferences!
                        .map((preference) =>
                            ColorChangingButton(text: preference))
                        .toList()),
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                  width: 390,
                  height: 60,
                  child: ElevatedButton(
                    onPressed: () async {
                      await Provider.of<AuthUserService>(context, listen: false)
                          .signOut();
                      if (!(await Provider.of<AuthUserService>(context,
                              listen: false)
                          .isUserLogged())) {
                        Provider.of<StackService>(context, listen: false)
                            .onItemTapped(-1, 5);
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                    ),
                    child: Text(
                      'Log Out',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ]),
        )));
  }
}

class ColorChangingButton extends StatefulWidget {
  const ColorChangingButton({Key? key, required this.text}) : super(key: key);

  final String text;

  @override
  _ColorChangingButtonState createState() => _ColorChangingButtonState();
}

class _ColorChangingButtonState extends State<ColorChangingButton> {
  bool _isPressed = false;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          _isPressed = !_isPressed;
        });
      },
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return Theme.of(context).primaryColor;
          },
        ),
      ),
      child: Text(
        widget.text,
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
