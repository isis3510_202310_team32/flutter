import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/views/screens/restaurant_list.dart';

import '../../view_models/connectivity_service.dart';
import '../../view_models/stack_service.dart';

class LocationList extends StatefulWidget {
  @override
  State<LocationList> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  void handleLostConnectivity() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<StackService>(context, listen: false).onItemTapped(2, 2);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!Provider.of<ConnectivityService>(context).isConnected) {
      handleLostConnectivity();
    }
    return WillPopScope(
      onWillPop: () async {
        Provider.of<StackService>(context, listen: false).onItemTapped(2, 2);
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Text(
              'Locations',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                  child: Text(
                    'Near Places for You',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
                Expanded(
                  child: RestaurantList(
                    location: true,
                  ),
                )
              ],
            ),
          )),
    );
  }
}
