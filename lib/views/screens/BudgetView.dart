import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/connectivity_service.dart';
import 'package:senefood/view_models/database_service.dart';
import 'package:senefood/view_models/event_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../view_models/restaurant_service.dart';
import '../../view_models/stack_service.dart';
import '../widgets/loading_indicator_dialog.dart';
import '../widgets/unavailable_dialog.dart';

class BudgetView extends StatefulWidget {
  const BudgetView({super.key});

  @override
  State<BudgetView> createState() => _BudgetViewState();
}

class _BudgetViewState extends State<BudgetView> {
  List<String> daysList = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday'
  ];
  List<String> daysSelected = [];
  double budgetPercentage = 0;
  TextEditingController priceController = TextEditingController();

  void handleChangeDay(value, day) {
    if (value) {
      setState(() {
        daysSelected.add(day);
      });
    } else {
      setState(() {
        daysSelected.remove(day);
      });
    }
  }

  void handleChangeBudget(value) {
    setState(() {
      budgetPercentage = value;
    });
  }

  void handleSubmit(bool connected) async {
    if (!connected) {
      showUnavailableDialog(context,
          title: 'Ups, there is no connection to internet',
          text:
              'The budget can\'t be calculated without an internet connection');
      return;
    }
    showLoadingIndicatorDialog(context);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int newBudget =
        int.parse(priceController.text == '' ? '0' : priceController.text);
    prefs.setInt('budget', newBudget);
    prefs.setDouble('budgetPercentage', budgetPercentage);
    int budgetValue = (newBudget * (1 - budgetPercentage / 100)).round();
    final EventService eventService = EventService();
    await eventService.sendEventBudgetConfigurationSaving(budgetPercentage);
    List<dynamic> dishesDays =
        Provider.of<RestaurantService>(context, listen: false)
            .selectDishesBudget(['fast food'], daysSelected, budgetValue);
    if (dishesDays.isNotEmpty) {
      await Provider.of<DatabaseService>(context, listen: false).deleteAll();
      for (int i = 0; i < dishesDays.length; i++) {
        await Provider.of<DatabaseService>(context, listen: false)
            .insert(dishesDays[i]);
      }
      Navigator.pop(context);
      Provider.of<StackService>(context, listen: false).onItemTapped(1, 1);
    } else {
      Navigator.pop(context);
      showUnavailableDialog(context,
          title: 'It was impossible to create a menu based on your options',
          text: 'Try to increment the budget value or to select less days.');
    }
  }

  @override
  Widget build(BuildContext context) {
    bool connected = Provider.of<ConnectivityService>(context).isConnected;
    return WillPopScope(
      onWillPop: () async {
        Provider.of<StackService>(context, listen: false).onItemTapped(1, 1);
        return false;
      },
      child: Material(
          child: Padding(
        padding: EdgeInsets.only(top: 50, right: 8, left: 8, bottom: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Spacer(),
            Text(
              'Price',
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
            NumberText(
              controller: priceController,
            ),
            Spacer(),
            Text(
              'Saving',
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
            Budget(
                budgetPercentage: budgetPercentage,
                changeBudgetPercentage: handleChangeBudget),
            Spacer(),
            Text(
              'Days of the Week',
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
            Switches(
              daysList: daysList,
              daysSelected: daysSelected,
              handleChangeDay: handleChangeDay,
            ),
            Spacer(),
            Spacer(),
            Spacer(),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.all(10),
                    child: ElevatedButton(
                      onPressed: () {
                        Provider.of<StackService>(context, listen: false)
                            .onItemTapped(1, 1);
                      },
                      style: ButtonStyle(
                        textStyle: MaterialStateProperty.all<TextStyle>(
                          TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w600,
                            fontSize: 17.0,
                          ),
                        ),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.black),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.only(
                              left: 50, right: 50, top: 15, bottom: 15),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ),
                      child: Text('Reset'),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: ElevatedButton(
                      onPressed: () {
                        handleSubmit(connected);
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).splashColor),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.only(
                              left: 50, right: 50, top: 15, bottom: 15),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ),
                      child: Text(
                        'Done',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 17.0,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Spacer(),
          ],
        ),
      )),
    );
  }
}

class Budget extends StatefulWidget {
  const Budget(
      {super.key,
      required this.budgetPercentage,
      required this.changeBudgetPercentage});

  final double budgetPercentage;
  final ValueChanged changeBudgetPercentage;

  @override
  State<Budget> createState() => _BudgetState();
}

class _BudgetState extends State<Budget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Slider(
            value: widget.budgetPercentage,
            min: 0,
            max: 100,
            divisions: 100,
            onChanged: widget.changeBudgetPercentage,
            activeColor: Theme.of(context).primaryColor,
            inactiveColor: Theme.of(context).hintColor,
          ),
        ),
        Text('${widget.budgetPercentage.toStringAsFixed(0)}%'),
      ],
    );
  }
}

class Switches extends StatefulWidget {
  const Switches(
      {super.key,
      required this.daysList,
      required this.daysSelected,
      required this.handleChangeDay});

  final List<String> daysList;
  final List<String> daysSelected;
  final Function handleChangeDay;

  @override
  State<Switches> createState() => _SwitchesState();
}

class _SwitchesState extends State<Switches> {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widget.daysList
            .map((day) => CustomSwitch(
                day, widget.daysSelected.contains(day), widget.handleChangeDay))
            .toList());
  }

  Widget CustomSwitch(String text, bool val, Function onChangeMethod) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 16, right: 16),
      child: Row(children: [
        Text(text),
        Spacer(),
        CupertinoSwitch(
            trackColor: Colors.white,
            activeColor: Theme.of(context).splashColor,
            value: val,
            onChanged: (newValue) {
              onChangeMethod(newValue, text);
            })
      ]),
    );
  }
}

class NumberText extends StatefulWidget {
  const NumberText({super.key, required this.controller});

  final TextEditingController controller;

  @override
  State<NumberText> createState() => _NumberTextState();
}

class _NumberTextState extends State<NumberText> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: TextField(
      controller: widget.controller,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      maxLength: 8,
      decoration: InputDecoration(
        prefixText: '\$ ',
        prefixStyle: TextStyle(color: Theme.of(context).primaryColor),
        hintText: 'COP',
        hintStyle: TextStyle(color: Colors.black),
      ),
    ));
  }
}
