import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ImageCardDiary extends StatelessWidget {
  final String imageUrl;
  const ImageCardDiary({required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(40),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.all(
          Radius.circular(4),
        ),
      ),
      width: 160.0,
      height: 160.0,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Padding(
          padding: EdgeInsets.all(10.0),
          child: CircularProgressIndicator(),
        ),
        errorWidget: (context, url, error) => Center(
          child: Icon(Icons.disabled_visible),
        ),
      ),
    );
  }
}
