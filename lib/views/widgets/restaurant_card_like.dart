import 'package:flutter/material.dart';
import 'package:senefood/view_models/restaurant_service.dart';
import 'package:senefood/views/widgets/loading_indicator_dialog.dart';

import 'image_card.dart';

class RestaurantCardLike extends StatefulWidget {
  final dynamic restaurant;
  final dynamic like;
  final Function(String, int) changeLike;

  const RestaurantCardLike(
      {super.key,
      required this.restaurant,
      required this.like,
      required this.changeLike});

  @override
  State<RestaurantCardLike> createState() => _RestaurantCardLike();
}

class _RestaurantCardLike extends State<RestaurantCardLike> {
  final RestaurantService restaurantService = RestaurantService();
  String userId = "dummyUser";

  dynamic chooseLikeBgColor() {
    if (widget.like > 0) {
      return MaterialStateProperty.all<Color>(Theme.of(context).primaryColor);
    } else {
      return MaterialStateProperty.all<Color>(Theme.of(context).disabledColor);
    }
  }

  dynamic chooseDislikeBgColor() {
    if (widget.like < 0) {
      return MaterialStateProperty.all<Color>(Theme.of(context).primaryColor);
    } else {
      return MaterialStateProperty.all<Color>(Theme.of(context).disabledColor);
    }
  }

  dynamic onPressLikeButton() async {
    showLoadingIndicatorDialog(context);
    int newLike = 0;
    if (widget.like <= 0) {
      newLike = 1;
      if (widget.like == 0) {
        await restaurantService.updateRecommendationScore(
            widget.restaurant.id, 1);
      } else if (widget.like <= -1) {
        await restaurantService.updateRecommendationScore(
            widget.restaurant.id, 2);
      }
    } else if (widget.like >= 1) {
      newLike = 0;
      await restaurantService.updateRecommendationScore(
          widget.restaurant.id, -1);
    }
    await restaurantService.updateRecommendation(
        userId, widget.restaurant.id, newLike);

    setState(() {
      widget.changeLike(widget.restaurant.id, newLike);
    });

    Navigator.pop(context);
  }

  dynamic onPressDislikeButton() async {
    showLoadingIndicatorDialog(context);
    int newLike = 0;
    if (widget.like >= 0) {
      newLike = -1;
      if (widget.like == 0) {
        await restaurantService.updateRecommendationScore(
            widget.restaurant.id, -1);
      } else if (widget.like == 1) {
        await restaurantService.updateRecommendationScore(
            widget.restaurant.id, -2);
      }
    } else if (widget.like <= -1) {
      newLike = 0;
      await restaurantService.updateRecommendationScore(
          widget.restaurant.id, 1);
    }
    await restaurantService.updateRecommendation(
        userId, widget.restaurant.id, newLike);
    setState(() {
      widget.changeLike(widget.restaurant.id, newLike);
    });
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        child: Container(
          height: 150.0,
          padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.black,
                width: 1.0,
              ),
            ),
          ),
          margin: EdgeInsets.all(10),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImageCard(imageUrl: widget.restaurant.imageName),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      widget.restaurant.name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    Row(
                      children: [
                        Icon(
                          widget.restaurant.stars >= 1
                              ? Icons.star
                              : Icons.star_border,
                          size: 20,
                          color: Colors.yellow,
                        ),
                        Icon(
                          widget.restaurant.stars >= 2
                              ? Icons.star
                              : Icons.star_border,
                          size: 20,
                          color: Colors.yellow,
                        ),
                        Icon(
                          widget.restaurant.stars >= 3
                              ? Icons.star
                              : Icons.star_border,
                          size: 20,
                          color: Colors.yellow,
                        ),
                        Icon(
                          widget.restaurant.stars >= 4
                              ? Icons.star
                              : Icons.star_border,
                          size: 20,
                          color: Colors.yellow,
                        ),
                        Icon(
                          widget.restaurant.stars >= 5
                              ? Icons.star
                              : Icons.star_border,
                          size: 20,
                          color: Colors.yellow,
                        )
                      ],
                    ),
                    Text(
                      widget.restaurant.categories.join(' • '),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 50,
                          child: ElevatedButton(
                              onPressed: onPressLikeButton,
                              style: ButtonStyle(
                                backgroundColor: chooseLikeBgColor(),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 0, vertical: 10),
                                child: Icon(
                                  Icons.thumb_up_outlined,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 50,
                          child: ElevatedButton(
                              onPressed: onPressDislikeButton,
                              style: ButtonStyle(
                                backgroundColor: chooseDislikeBgColor(),
                                shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 0, vertical: 10),
                                child: Icon(
                                  Icons.thumb_down_outlined,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              )),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      )
    ]);
  }
}
