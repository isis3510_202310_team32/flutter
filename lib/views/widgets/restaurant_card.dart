import 'package:flutter/material.dart';
import 'package:senefood/models/restaurant.dart';
import 'package:senefood/view_models/event_service.dart';
import 'package:senefood/views/screens/restaurant_detail.dart';

import 'image_card.dart';

class RestaurantCard extends StatefulWidget {
  final Restaurant restaurant;
  final dynamic distance;
  const RestaurantCard(
      {super.key, required this.restaurant, required this.distance});

  @override
  State<RestaurantCard> createState() => _RestaurantCard();
}

class _RestaurantCard extends State<RestaurantCard> {
  final EventService eventService = EventService();

  dynamic getMoney() {
    List<dynamic> dishes = widget.restaurant.dishes;
    dynamic cost = 0;
    dynamic count = 0;
    for (var dish in dishes) {
      cost += dish['price'];
      count++;
    }
    var average = cost / count;
    if (average < 15000) {
      return "\$";
    } else if (average < 20000) {
      return "\$\$";
    } else {
      return "\$\$\$";
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        List<dynamic> categories = widget.restaurant.categories
            .map((category) => category.toString())
            .toList();
        eventService.logSelectContentEvent(categories);
        eventService.logEventRestaurantPerDate(widget.restaurant.name);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  RestaurantDetail(restaurant: widget.restaurant)),
        );
      },
      child: Row(children: [
        Expanded(
          child: Container(
            height: 150.0,
            padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
            ),
            margin: EdgeInsets.all(10),
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageCard(imageUrl: widget.restaurant.imageName),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        widget.restaurant.name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            widget.restaurant.stars >= 1
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.restaurant.stars >= 2
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.restaurant.stars >= 3
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.restaurant.stars >= 4
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.restaurant.stars >= 5
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            'Distance:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          Text(
                            widget.distance != null
                                ? '${widget.distance} m'
                                : 'Loading...',
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            'Cleanness score:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                          Text(
                            '${widget.restaurant.cleannessScore}/10',
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        widget.restaurant.categories.join(' • '),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      Text(
                        getMoney(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      ]),
    );
  }
}
