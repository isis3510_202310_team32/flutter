import 'package:flutter/material.dart';

import 'image_card.dart';

class DishCard extends StatefulWidget {
  final dynamic dish;
  const DishCard({super.key, required this.dish});

  @override
  State<DishCard> createState() => _DishCardState();
}

class _DishCardState extends State<DishCard> {
  @override
  Widget build(BuildContext context) {
    // print(widget.dish);
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 160.0,
            padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
            ),
            margin: EdgeInsets.all(10),
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageCard(imageUrl: widget.dish['imageName']),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        widget.dish['name'],
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            'Prep. time: ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            ),
                          ),
                          Text(
                            widget.dish['preparationTime'].toString(),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          Text(
                            'min',
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            widget.dish['stars'] >= 1
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.dish['stars'] >= 2
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.dish['stars'] >= 3
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.dish['stars'] >= 4
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          ),
                          Icon(
                            widget.dish['stars'] >= 5
                                ? Icons.star
                                : Icons.star_border,
                            size: 20,
                            color: Colors.yellow,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            '\$',
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          Text(
                            widget.dish['price'].toStringAsFixed(0),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
