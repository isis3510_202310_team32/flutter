import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:senefood/view_models/restaurant_service.dart';

import '../../models/filters.dart';
import '../../models/restaurant.dart';
import '../../models/sorting_option.dart';

class RestaurantFilters extends StatefulWidget {
  final List<SortingOption> sortingOptions;
  final Filters filters;

  const RestaurantFilters(
      {required this.filters, required this.sortingOptions});

  @override
  _RestaurantFilters createState() => _RestaurantFilters();
}

class _RestaurantFilters extends State<RestaurantFilters> {
  List<String> selectedCategories = [];
  String sortingSelected = "";
  dynamic minPrice = TextEditingController();
  dynamic maxPrice = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      selectedCategories = widget.filters.selectedCategories;
      sortingSelected = widget.filters.sortingSelected;
      if (widget.filters.minPrice != null) {
        minPrice.text = widget.filters.minPrice.toString();
      }
      if (widget.filters.maxPrice != null) {
        maxPrice.text = widget.filters.maxPrice.toString();
      }
    });
  }

  dynamic onSubmit() {
    Function setAttribute =
        Provider.of<RestaurantService>(context, listen: false)
            .setAttributeFilters;
    setAttribute('selectedCategories', selectedCategories);
    setAttribute('sortingSelected', sortingSelected);
    setAttribute(
        'minPrice', minPrice.text != '' ? double.parse(minPrice.text) : null);
    setAttribute(
        'maxPrice', maxPrice.text != '' ? double.parse(maxPrice.text) : null);
    Provider.of<RestaurantService>(context, listen: false)
        .saveFiltersToPreferences();
    Navigator.pop(context);
  }

  dynamic onReset() {
    setState(() {
      selectedCategories = [];
      sortingSelected = "";
      minPrice.text = "";
      maxPrice.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Restaurant> restaurants =
        Provider.of<RestaurantService>(context).restaurants;
    List<String> categories =
        Provider.of<RestaurantService>(context).getCategories(restaurants);
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Color(0xFFF9F9F9)),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                    child: Row(children: [
                  Icon(
                    Icons.filter_list,
                  ),
                  SizedBox(width: 5),
                  Text(
                    'Filters',
                    style: TextStyle(fontSize: 20),
                  )
                ])),
                IconButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      Navigator.pop(context); // dismiss the bottom sheet
                    },
                    icon: Icon(Icons.cancel_outlined))
              ],
            ),
            SizedBox(
              height: 24,
            ),
            Text(
              "Filter by:",
              style: TextStyle(fontSize: 28),
            ),
            SizedBox(
              height: 28,
            ),
            Text(
              "Price",
              style: TextStyle(fontSize: 22),
            ),
            SizedBox(
              height: 28,
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "\$",
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    width: 100,
                    child: TextField(
                      controller: minPrice,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      style: TextStyle(color: Color.fromRGBO(60, 60, 67, 0.6)),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(10),
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Min',
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(60, 60, 67, 0.6)),
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ),
                  ),
                  Text(
                    "-",
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    width: 100,
                    child: TextField(
                      controller: maxPrice,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      style: TextStyle(color: Color.fromRGBO(60, 60, 67, 0.6)),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(10),
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Max',
                        hintStyle:
                            TextStyle(color: Color.fromRGBO(60, 60, 67, 0.6)),
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(5)),
                      ),
                    ),
                  ),
                  Text(
                    "COP",
                    style: TextStyle(fontSize: 22),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              "Category",
              style: TextStyle(fontSize: 22),
            ),
            SizedBox(
              height: 28,
            ),
            SizedBox(
                height: 30,
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.horizontal,
                  itemCount: restaurants.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: ToggleButtons(
                        selectedColor: Colors.white,
                        fillColor: Color(0xFFFE6454),
                        borderRadius: BorderRadius.circular(15),
                        textStyle: TextStyle(color: Colors.black),
                        selectedBorderColor: Color(0xFFFE6454),
                        isSelected: [
                          selectedCategories.contains(categories[index])
                        ],
                        onPressed: (int selectedIndex) {
                          setState(() {
                            if (selectedCategories
                                .contains(categories[index])) {
                              selectedCategories.remove(categories[index]);
                            } else {
                              selectedCategories.add(categories[index]);
                            }
                          });
                        },
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Text(categories[index]),
                          ),
                        ],
                      ),
                    );
                  },
                )),
            SizedBox(
              height: 28,
            ),
            Text(
              "Sort by:",
              style: TextStyle(fontSize: 22),
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                height: 100,
                child: ListView.builder(
                  itemCount: widget.sortingOptions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          sortingSelected = widget.sortingOptions[index].value;
                        });
                        // widget.onOptionSelected(widget.sortingOptions[index]);
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 6.0),
                        child: Text(
                          widget.sortingOptions[index].name,
                          style: TextStyle(
                            fontWeight: sortingSelected ==
                                    widget.sortingOptions[index].value
                                ? FontWeight.bold
                                : FontWeight.normal,
                          ),
                        ),
                      ),
                    );
                  },
                )),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                    onPressed: onReset,
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(14.0),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 60, vertical: 14),
                        textStyle: TextStyle(fontWeight: FontWeight.bold)),
                    child: Text('Reset')),
                ElevatedButton(
                    onPressed: onSubmit,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color(0xFFB9F6D9),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.0),
                      ),
                      padding:
                          EdgeInsets.symmetric(horizontal: 60, vertical: 14),
                    ),
                    child: Text(
                      'Done',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
