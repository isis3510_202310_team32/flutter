import 'package:flutter/material.dart';
import 'package:senefood/utils/location.dart';
import 'package:senefood/view_models/event_service.dart';
import 'package:senefood/view_models/reviews_view_model.dart';
import 'package:senefood/views/screens/reviews_view.dart';

import 'image_card.dart';

class RestaurantCardReviews extends StatefulWidget {
  final dynamic restaurant;

  const RestaurantCardReviews({super.key, required this.restaurant});

  @override
  State<RestaurantCardReviews> createState() => _RestaurantCardReviews();
}

class _RestaurantCardReviews extends State<RestaurantCardReviews> {
  final EventService eventService = EventService();
  final ReviewsViewModel reviewsViewModel = ReviewsViewModel();
  dynamic distance = 0;

  dynamic getMoney() {
    List<dynamic> dishes = widget.restaurant.dishes;
    dynamic cost = 0;
    dynamic count = 0;
    for (var dish in dishes) {
      cost += dish['price'];
      count++;
    }
    var average = cost / count;
    if (average < 15000) {
      return "\$";
    } else if (average < 20000) {
      return "\$\$";
    } else {
      return "\$\$\$";
    }
  }

  dynamic getDistance(restaurant) async {
    dynamic newDistance = await distanceFromActualLocation(restaurant);
    setState(() {
      distance = newDistance;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDistance(widget.restaurant);
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        child: Container(
          height: 160.0,
          padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.black,
                width: 1.0,
              ),
            ),
          ),
          margin: EdgeInsets.all(10),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImageCard(imageUrl: widget.restaurant.imageName),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Distance:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        Text(
                          '$distance m',
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Cleanness score:',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        Text(
                          '${widget.restaurant.cleannessScore}/10',
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      widget.restaurant.categories.join(' • '),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      getMoney(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    Spacer(),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ReviewsView(
                                reviewsViewModel: reviewsViewModel,
                                restaurantId: widget.restaurant.id,
                              ),
                            ));
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).primaryColor),
                        padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ),
                      child: Text(
                        'Reviews',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      )
    ]);
  }
}
