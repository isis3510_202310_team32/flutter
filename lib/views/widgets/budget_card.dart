import 'package:flutter/material.dart';

import 'image_card.dart';

class BudgetCard extends StatefulWidget {
  const BudgetCard({Key? key, required this.dish}) : super(key: key);

  final dynamic dish;

  @override
  State<BudgetCard> createState() => _BudgetCardState();
}

class _BudgetCardState extends State<BudgetCard> {
  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        child: Container(
          height: 150.0,
          padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.black,
                width: 1.0,
              ),
            ),
          ),
          margin: EdgeInsets.all(10),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ImageCard(imageUrl: widget.dish['image']),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.dish['name'],
                      style: TextStyle(
                        fontSize: 23,
                      ),
                    ),
                    Text(
                      widget.dish['restaurant'],
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    Text(
                      '\$ ${widget.dish['price']} COP',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      )
    ]);
  }
}
