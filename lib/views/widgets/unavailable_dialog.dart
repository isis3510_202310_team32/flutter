import 'package:flutter/material.dart';

Future<void> showUnavailableDialog(context, {title, text}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        // <--  HERE
        title: Text(title ?? 'Ups, no internet connection'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(
                  text ?? 'This feature can only be used with an active internet connection. Connect to the internet and try again later.'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
