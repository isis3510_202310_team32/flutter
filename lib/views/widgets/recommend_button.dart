import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senefood/views/widgets/unavailable_dialog.dart';

import '../../view_models/permissions_service.dart';

class RecommendButton extends StatelessWidget {
  final Function onPressed;
  final IconData icon;
  final String title;
  final bool disabled;

  RecommendButton(
      {required this.onPressed,
      required this.title,
      required this.icon,
      required this.disabled});

  @override
  Widget build(BuildContext context) {
    PermissionsService permissionsService =
        Provider.of<PermissionsService>(context);
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(10),
        child: ClipRect(
          child: ElevatedButton(
            onPressed: () {
              if (!permissionsService.locationPermission &&
                  title == 'Location') {
                showUnavailableDialog(context,
                    title: 'Ups, the location is off',
                    text:
                        'The location feature can only be used with an active location. Turn on the location and try again later');
              } else if (disabled) {
                showUnavailableDialog(context);
              } else {
                onPressed();
              }
            },
            style: ButtonStyle(
              padding: MaterialStateProperty.all<EdgeInsets>(
                EdgeInsets.symmetric(horizontal: 10),
              ),
              backgroundColor: disabled
                  ? MaterialStateProperty.all<Color>(
                      Theme.of(context).disabledColor)
                  : MaterialStateProperty.all<Color>(
                      Theme.of(context).primaryColor),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
            child: Row(
              children: [
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
                Flexible(child: Icon(icon, size: 100)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
