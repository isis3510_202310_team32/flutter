import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../view_models/connectivity_service.dart';
import '../../view_models/stack_service.dart';

class BottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.restaurant),
              label: 'Restaurants',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.paid),
              label: 'Budget',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.auto_awesome,
              ),
              label: 'Recommend',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Profile',
            ),
          ],
          currentIndex: Provider.of<StackService>(context).tabIndex,
          selectedItemColor: Color(0xFFFE6454),
          unselectedItemColor: Colors.grey,
          onTap: (index) => {
            Provider.of<StackService>(context, listen: false).onTabChange(index)
          },
        ),
        !Provider.of<ConnectivityService>(context).isConnected
            ? Container(
                color: Color(0xFFFE6454),
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Text('Senefood offline',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                  ),
                ),
              )
            : Container()
      ],
    );
  }
}
